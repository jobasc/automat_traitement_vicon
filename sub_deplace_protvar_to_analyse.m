%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           d�placement des fichiers protocole et variable
%                           en r�pertoire analyse
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - d�placement des fichiers prot et var du r�pertoire vicon vers le r�pertoire
%   analyse, dont la structure peut �tre lue avec mouvement
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire de destination
%
% En sortie:
%   - r�pertoire analyse rempli avec les fichiers
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


log_var{ilog} = ['xxxxxxxxx  copie prot et var dans rep analyse'] ; ilog = ilog+1;

if ~isempty( dir( fullfile( rep_prot,'*.prot') ) )
    copyfile(fullfile( rep_prot,'*.prot'), rep_analyse) ;
else
    disp(['attention, pas de fichier prot dans : ' , rep_prot]);
    log_var{ilog} = ['attention, pas de fichier prot dans : ' , rep_prot] ; ilog = ilog+1;
end

if ~isempty( dir( fullfile( rep_prot,'*.mod') ) )
    copyfile(fullfile( rep_prot,'*.mod'), rep_analyse) ;
else
    disp(['attention, pas de fichier mod dans : ' , rep_prot]);
    log_var{ilog} = ['attention, pas de fichier mod dans : ' , rep_prot] ; ilog = ilog+1;
end

if ~isempty( dir( fullfile( rep_prot,'*.var') ) )
    copyfile(fullfile( rep_prot,'*.var'), rep_analyse) ;
else
    disp(['attention, pas de fichier var dans : ' , rep_prot]);
    log_var{ilog} = ['attention, pas de fichier var dans : ' , rep_prot] ; ilog = ilog+1;
end

if ~isempty( dir( fullfile( rep_prot,'*.plat') ) )
    copyfile(fullfile( rep_prot,'*.plat'), rep_analyse) ;
else
    disp(['attention, pas de fichier plat dans : ' , rep_prot]);
    log_var{ilog} = ['attention, pas de fichier plat dans : ' , rep_prot] ; ilog = ilog+1;
end