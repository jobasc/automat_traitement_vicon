%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      D�tection des cycles
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - d�tection des cycles automatiquement par info plateforme de force
%   - ne fonctionne pour l'instant que pour la marche sur ptf
%
% En entr�e:
%   - kinematic_data, actions_meca
%   - R_SALLE_VIC_t
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "sub_kinetics.m"
%
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       modifications
%
% bascou 2015 11 05 cr�ation
%

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
disp('... D�tection des cycles') ;

consecutive_stances = appui_plateformes(actions_meca);
consecutive_stances(:,find(isnan(consecutive_stances(1,:)))) = [];

if ~isfield(protocole,'SITUATIONS')
    % res variable d�fini par d�faut pour la marche � plat
    % avec aller retour (1,-1)
    res_variable.cycle.D = {'-1','1'};
    res_variable.cycle.G = {'-1','1'};
elseif isfield(protocole,'SITUATIONS');
    res_variable = sel_varfile.(mysituation) ;
end

if ~isfield(protocole,'CYCLE')
    protocole.CYCLE.D = {'points3D_t','CALD'};
    protocole.CYCLE.G = {'points3D_t','CALG'};
end


% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% 
%           d�tection du sens de marche
%
%   diff�rence entre les coord x entre premier et dernier instant de 
%   l'acquisition d'un segment utilis� pour l'orientation
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
pos_seg_orient_tmp1 = MP_Rvic_Ranat_t.( seg_orient{1} )(1:3,4,:) ;
pos_seg_orient_tmp2 = pos_seg_orient_tmp1(:,:,~isnan(pos_seg_orient_tmp1(1,1,:) ) ) ;    % �l�ments non nan
%  sens = axe x = position finale - position initiale
x_axe_tmp = round( (pos_seg_orient_tmp2(:,:,end) - pos_seg_orient_tmp2(:,:,1)) / max( abs (pos_seg_orient_tmp2(:,:,end) - pos_seg_orient_tmp2(:,:,1) ) ) ) ;      

% prise en compte de l'orientation du repere vicon
% (Rotation de 90 dans la salle de l'ENSAM
axe_R_VIC = find( abs( x_axe_tmp ) );
sens = sign(x_axe_tmp(axe_R_VIC,1));

% % xxxxxxxxxx          partie annul�e
% % le nan mean ne marche pas quand on perd le bassin au
% % cours de l'acqui probleme � voir
% R_SALLE_VIC_moy = round( nanmean(R_SALLE_VIC_t,3) );
% 
% % prise en compte de l'orientation du repere vicon
% % (Rotation de 90 dans la salle de l'ENSAM
% axe_R_VIC = find(abs(R_SALLE_VIC_moy(:,1)));
% sens = sign(R_SALLE_VIC_moy(axe_R_VIC,1));
% % xxxxxxxxxxxxxx          fin partie annul�e

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% 
%           It�ration sur les cot�s
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
sides = fieldnames(protocole.CYCLE);

for i_side = 1:length(sides)
    myside = sides{i_side};
    variable = protocole.CYCLE.(myside){1};
    champ = protocole.CYCLE.(myside){2};
    variable_controle = load(fullfile(mytrialdir, variable) );
    point_controle = variable_controle.(variable).(champ);
    
    % le fichier variable contient les noms de champ des
    % cycles et les sens associ�s on regarde pour le cote
    % concern� si le sens de marche identifi� est � prendre
    % en compte
    noms_champ_cycle = fieldnames(res_variable.cycle);
    ind_champ_cycle_courant = strcmp(myside,noms_champ_cycle);
    if isempty(find(ind_champ_cycle_courant,1))
        [~,sousliste_champ_cycle_courant] = strtok(noms_champ_cycle,myside);
        ind_champ_cycle_courant = strcmp(myside,sousliste_champ_cycle_courant);
        
    end;
    champ_cycle_courant = noms_champ_cycle(ind_champ_cycle_courant);
    % recherche des cycles et identification du numero de plateforme associ�
    for i_champ_cycle_courant = 1:length(champ_cycle_courant)
        mycurr_cycle = champ_cycle_courant{i_champ_cycle_courant} ;
        if ~isempty(find(strcmp(num2str(sens), res_variable.cycle.(mycurr_cycle)),1))
            [cycle.(['appui', mycurr_cycle]),cycle.(mycurr_cycle)] = extrait_cycles_forceplate(point_controle,consecutive_stances,axe_R_VIC);
            if isnan(cycle.(['appui',mycurr_cycle]))
                disp(['pas de cycle ' mycurr_cycle]) ;
            end;
        end;
    end;
    
end;

[actions_meca] = affecte_actions_meca_cycle(actions_meca,cycle);

save( fullfile( mytrialdir, 'actions_meca.mat'), 'actions_meca') ;
save( fullfile( mytrialdir, 'cycle.mat'), 'cycle') ;

disp( 'fin de d�tection des cycles');