%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calcul de la moyenne
%
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - expression de la moyenne des donn�es
%
% En entr�e:
%   - xxxxx
%
% En sortie:
%   - xxxxx
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m",
% 'Sub_posttreat"
%
%   - lst_variables_desc, lst_variables, lst_trials
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat


% *** liste des mouvements
% % % movement_list = get(findobj('tag','liste_marches'),'string');

% listage des param�tres et r�sultats pr�sents (res* et param*)

lst_var_tmp = dir( fullfile(rep_res_cin, 'res_*.*') ) ;
lst_param_tmp = dir(  fullfile(rep_res_cin, 'param*.*') );
lst_var_param = {lst_var_tmp(:).name, lst_param_tmp(:).name} ;

% it�ration sur les variables et param�tres
for i_variable = 1:length(lst_var_param)
    myvariable_file = lst_var_param{i_variable} ;
    myvariable_nom = regexprep(myvariable_file,'.mat', '') ;
    
%     chargement de la variable
%     load( fullfile(rep_res_cin, myvariable_file) );
    eval(['myvar_tmp=' myvariable_nom ';']);

    % calcul de la moyenne
    matrice_moyenne = moyenne(myvar_tmp) ;
    
    % assignation de la moyenne calcul�e � une variable
    eval([['moy_' myvariable_nom] '=matrice_moyenne;']);
    
    % sauvegarde
    save( fullfile(rep_res_cin, ['moy_' myvariable_nom] ),['moy_' myvariable_nom]);
    
end;
disp('Moyenne calcul�e')


% xxxxxxxxxxxxxxxxxxxxxxxxxxx
%       mise � jour de la liste des variables moyennes
lst_var_param_moy = lst_var_param ;
for ivarmoy = 1:length(lst_var_param_moy)
    lst_var_param_moy{ivarmoy} = ['moy_' lst_var_param{ivarmoy}] ;
end


