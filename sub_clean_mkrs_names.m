
function [cleaned_lst_mkrs] = sub_clean_mkrs_names(lst_mkrs_to_rename, lst_mkrs_cible, nom_patient)

% Objectif:
%   - Il arrive que le nom du sujet soit inclus dans le nom des marqueurs au
% moment de la labellisation sous vicon.
%   - On enleve le nom du sujet de la liste des noms de marqueurs
%
% En entr�e:
%   - liste de marqueurs (cell n_marqueurs * 1)
%
% En sortie:
%   - liste de marqueurs renomm�s (cell n_marqueurs * 1)
%

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               mise sous forme de vecteur colonne si besoin
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
if size(lst_mkrs_to_rename,1) < length(lst_mkrs_to_rename)
    lst_mkrs_to_rename = lst_mkrs_to_rename';
end

    cleaned_lst_mkrs = lst_mkrs_to_rename ;

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               suppression nom patient du nom marqueur
%
%     (si la liste des marqueurs cible et le nom patient sont renseign�s)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   
if exist('lst_mkrs_cible', 'var')
    
    if size(lst_mkrs_cible,1) < length(lst_mkrs_cible)
        lst_mkrs_cible = lst_mkrs_cible';
    end
    
    %   calcul des possibles noms de marqueurs en ajoutant lepr�fixe
    labels_prop = strcat( repmat([nom_patient '_'], size(lst_mkrs_cible),1), lst_mkrs_cible ) ;
    
    %   index des correspondance entre marqueurs de l'acquisition et liste retravaill�e
    INDEXMATCH =  matchcell( lst_mkrs_to_rename, labels_prop ) ;

    
    %   Suppression du pr�fixe des marqueurs trouv�s
    cleaned_lst_mkrs( INDEXMATCH ~= 0) = lst_mkrs_cible( INDEXMATCH( INDEXMATCH~=0 ) ) ;
    
end

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%       suppression des pr�fixes des libell�s
%           
%   d�tection auto du pr�fixe (plus longue chaine de caract�re commune)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% Calcul du pr�fixe commun aux marqueurs et proposition de liste de remplacement
[ cleaned_lst_mkrs, model_name ] = search_modelname_from_mkrlst( cleaned_lst_mkrs ) ;



