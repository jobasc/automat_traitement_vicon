%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%      Modifie le label des marqueurs dans tous les fichiers .C3D
%       dans le cas o� de nouveaux labels sont indiqu�s dans un
%                           fichier.relabel
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - dans les C3D du r�pertoire cible (repertoire d'analyse), modifier les
%   labels des marqueurs
%   - uniquement si un fichier .RELABEL existe
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire contenant les fichiers .plat des situations � traiter.
%       . Contiennent notamment la position des plateformes de force
%       . FORMAT: devers_CERAH.plat ou pente5_CERAH.plat ou pente5_IRR.plat
%   - r�pertoire de destination
%   - choix des patients � traiter
%   - choix des situations � traiter
%
% En sortie:
%   - r�pertoire "scanbase" � c�t� du r�pertoire initial,
%   	dont le nom est le nom du repertoire initial, avec "scanbase" en
%   	suffixe
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - lst_situations  : toutes situations (plat, d�vers, pente, etc) seront trait�es

log_var{ilog} = ['xxxxxxxxx  pr�traitement fichiers c3d'] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx      
%           Chargement �ventuel du fichier de nouvelle labellisation
% utilisation du premier fichier .relabel trouv�
tmp_lst = dir(fullfile(rep_platfiles,'*.relabel')) ;
if ~isempty(tmp_lst)
    newlabels = lire_fichier_prot(rep_platfiles,tmp_lst(1).name);
    log_var{ilog} = ['... chargement du fichier de relabellisation'] ; ilog = ilog+1;
else
    newlabels = {};
end


% xxxxxxxxxxx   It�ration sur les situations (plat, Statique, d�vers, etc.)
for i_situation = 1:size(lst_situations,2)
    mysituation = lst_situations{i_situation} ;
    mysit_rep = fullfile( rep_patient, mysituation) ;
    log_var{ilog} = ['xxxxxxxxxxx    relabel C3D     Situation ' mysituation] ; ilog = ilog+1;
    
    if strcmp(mysituation, 'Statique')
        mysit_rep_cible = fullfile( patient_courant, 'fichiers_reference') ;
    else
        mysit_rep_cible = fullfile( patient_courant, mysituation, 'fichiers_cinemat') ;
    end
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %                  relabel des fichiers cibles si variable newlabels non vide
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % xxxxxxxx          relabel des fichiers si variable newlabels non vide
    if ~isempty( newlabels)
        tmp_lst = dir( fullfile( mysit_rep_cible, '*.c3d' )) ;
        lst_files_to_read = {tmp_lst(:).name};
        
        log_var{ilog} = ['... Relabellisation des fichiers de ' mysit_rep_cible] ; ilog = ilog+1;
        
        for i_file=1:length(tmp_lst)
            file_to_read =  lst_files_to_read{i_file} ;
            %             relabel_c3d_irr_vers_lbm(file_to_read, mysit_rep_cible, newlabels) ;
            errorlabels = relabel_c3d(fullfile( mysit_rep_cible, file_to_read) , newlabels) ;
            log_var{ilog} = ['... Erreurs de label: ' errorlabels] ; ilog = ilog+1;
        end
    end   % if ~isempty( newlabels)
    
end

log_var{ilog} = ['xxxxxxxxx  fin relabel fichiers c3d'] ; ilog = ilog+1;

