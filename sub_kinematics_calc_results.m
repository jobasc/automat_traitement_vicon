%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Cin�matique - calcul des r�sultats
%
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
% Objectif:
%   - Calcul des r�sultats de la cin�matique
%
% En entr�e:
%   - Phi_Ranat: matrices de pseudo inertie en rep�re anatomique
%   - res_effort
%   - res_moment
%   - JsRo
%   - W_ANAT_VIC_t
%   - V_CDG_VIC_t
%   - actions_meca
%   - Phi_Ranat
%   - list_segments  = protocole.segments
%   - lst_variables
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%       modifications
% bascou 2015 11 04 : cr�ation
%

disp('... calcul des variables cin�matiques de sortie');

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           calcul des distances entre les points d'insertion
%                   (communs � 2 segments)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% M�thode:
%   chaque point d'insertion est calcul� gr�ce au rep�re local du segment
% et au d�placement de marqueurs des points de ce rep�re.
%   Il peut donc apparaitre une diff�rence entre les positions du
% m�me marqueur, calcul� de deux fa�ons diff�rentes.
%   On veut ici conna�tre le d�calage de position entre le marqueur
% d'insertion, selon qu'il est consid�r� comme faisant partie d'un
% segment ou d'un autre.

if ~isempty( find( strcmp(lst_variables, 'res_distances_t'), 1))
    
    noms_points = fieldnames(points3D_t);
    
    disp('... Calcul des distances entre les insertions');
    warning off ;
    
    %   D�tection des points d'insertion (communs � deux segments)
    %       - Si un marqueur est commun � deux segments, c'est une insertion
    % entre ces deux segments
    %       - � chaque segment, comparaison de sa liste de marqueurs �
    %       la liste de marqueurs des segments suivants pour d�tecter
    %       leur marqueur commun
    % %         list_segments = fieldnames(protocole.SEGMENTS);
    liste_insertion={};
    for i_seg = 1:length(list_segments)-1   % it�ration sur segment de r�f�rence
        lst_mkr_segref = protocole.SEGMENTS.(list_segments{i_seg});  % liste de marqueurs du segment de r�f�rence
        
        for j_seg = i_seg+1:length(list_segments)  % it�ration sur les segments de comparaison
            lst_mkr_segcomp = protocole.SEGMENTS.(list_segments{j_seg}); % liste de marqueurs du segment de comparaison
            [row,col] = find(compare_liste(lst_mkr_segref, lst_mkr_segcomp));  % Marqueurs communs aux deux segments
            
            % remplissage de la liste des mkrs d'insertion s'il y a lieu
            if ~isempty(row)
                liste_insertion = [liste_insertion,lst_mkr_segref{row}];
            end;
        end     % j_seg: segment de comparaison
    end;
    for i_insert = 1:length(liste_insertion)
        
        
        %  xxxxx  A completer si plus de deux points d'insertion  xxxxxxxxxx
        points_insert = noms_points(strncmp(liste_insertion{i_insert}, noms_points, length(liste_insertion{i_insert})));
        res_distances_t.(liste_insertion{i_insert})=[zeros(length(points3D_t.(points_insert{1})),2),norm2(points3D_t.(points_insert{1})-points3D_t.(points_insert{2}))];
        
        %   Sauvegarde distances intersegmentaires
        save( fullfile( mytrialdir,'res_distances_t.mat'), 'res_distances_t') ;
    end;     % for i_insert
end  % if isempty variable res_distance_t

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           angles intersegmentaires - absolus et relatifs
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% *** angles et angles relatifs
if ~isempty( find( strcmp(lst_variables, 'res_angles_t'), 1)) || ~isempty( find( strcmp(lst_variables, 'res_var_angle_t'), 1))
    disp('... calcul des angles');
%     warning off;
    list_seg = fieldnames(MP_Rvic_Ranat);
    for iseg = 1:length(list_seg)
        myseg = list_seg{iseg} ;
        MP_Rsalle_Ranat.(myseg) = MP_Rvic_Rsalle \ MP_Rvic_Ranat.(myseg);  % mldivide : equivalent � inv(R_SALLE_VIC) * R_ANAT_VIC.(myseg)
        MP_Rsalle_Ranat_t.(myseg)=[];
        for itps = 1:size(R_ANAT_VIC_t.(myseg),3)
            MP_Rsalle_Ranat_t.(myseg)(:,:,itps) = MP_Rvic_Rsalle_t(:,:,itps) \ MP_Rvic_Ranat_t.(myseg)(:,:,itps);  % equivalent � inv(R_SALLE_VIC_t(:,:,itps)) * R_ANAT_VIC_t.(myseg)(:,:,itps)
        end % for itps
    end % for iseg
    
    % R_ANAT_SALLE_t remplac� par MP_Rsalle_Ranat
    [res_angles_t, res_var_angle_t] = angles(MP_Rsalle_Ranat_t, MP_Rsalle_Ranat, protocole);
    
    % sauvegarde des r�sultats
    save( fullfile( mytrialdir, 'res_angles_t.mat') , 'res_angles_t' ) ;
    save( fullfile( mytrialdir, 'res_var_angle_t.mat'), 'res_var_angle_t' ) ;
end

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           translations intersegmentaires - absolues et relatives
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

if ~isempty( find( strcmp(lst_variables, 'res_translations_t'), 1))
    [res_var_translations_t,res_translations_t] = translations(MP_Rsalle_Ranat_t,MP_Rsalle_Ranat,protocole);
    save( fullfile( mytrialdir, 'res_var_translations_t.mat' ), 'res_var_translations_t' ) ;
    save( fullfile( mytrialdir, 'res_translations_t.mat'), 'res_translations_t' ) ;
end;
%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           Normalisation des coordonn�es 3D des points
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% anatomiques
res_points3D=[];

if ~isempty(P_ANAT_VIC_t) && ~isempty( find( strcmp(lst_variables, 'res_points3D'), 1))
    
    disp('...calcule des points 3D');
    res_points3D = P3D_VIC_2_SALLE(points3D_t, R_SALLE_VIC_t);
    
    save( fullfile( mytrialdir, 'res_points3D.mat'), 'res_points3D' ) ; 
    
end;


% ******* positions absolues et relatives des segments
if ~isempty( find( strcmp(lst_variables, 'res_pos_abs_t'), 1)) && ~isempty( find( strcmp(lst_variables, 'res_pos_rel_t'), 1))
    disp('... positions absolues et relatives des rep�res segments');
    % ajout jba - 03/01/2011 : calcul de positions
    % relatives
    res_pos_abs_t = calcul_position_dynamique(POS_ABS_t);
    res_pos_rel_t = calcul_position_dynamique(POS_REL_t);
    save( fullfile( mytrialdir, 'res_pos_abs_t.mat'), 'res_pos_abs_t' ) ;
    save( fullfile( mytrialdir, 'res_pos_rel_t.mat'), 'res_pos_rel_t' ) ;
    disp('... . OK positions absolues et relatives des rep�res segments');
end

% **** vitesses absolues lin�aires et angulaires
if ~isempty( find( strcmp(lst_variables, 'res_V_lin_abs_t'), 1)) || ~isempty( find( strcmp(lst_variables, 'res_V_ang_abs_t'), 1)) || ~isempty( find( strcmp(lst_variables, 'res_V_ang_SALLE_t'), 1))
    disp('... vitesses absolues lin et ang des rep�res segments');
    % ajout jba 29/11/2011 : calcul de vitesses
    [res_V_lin_abs_t,res_V_ang_abs_t] = sortie_calc_V_segments(W_ANAT_VIC_t);
    res_V_ang_SALLE_t=P3D_VIC_2_SALLE(res_V_ang_abs_t,R_SALLE_VIC_t);
    save( fullfile( mytrialdir, 'res_V_lin_abs_t.mat'), 'res_V_lin_abs_t' ) ;
    save( fullfile( mytrialdir, 'res_V_ang_abs_t.mat'), 'res_V_ang_abs_t' ) ;
    save( fullfile( mytrialdir, 'res_V_ang_SALLE_t.mat'), 'res_V_ang_SALLE_t' )  ;
    disp('... . OK vitesses absolues lin et ang des rep�res segments');
end


% **** vitesses relatives lin�aires et angulaires
if ~isempty( find( strcmp(lst_variables, 'res_V_lin_rel_t'), 1)) && ~isempty( find( strcmp(lst_variables, 'res_V_ang_rel_t'), 1))
    disp('... vitesses relatives lin et ang des rep�res segments');
    % ajout jba 29/11/2011 : calcul de vitesses
    
    [res_V_lin_rel_t,res_V_ang_rel_t] = sortie_calc_V_articulations(W_ANAT_VIC_t,R_ANAT_VIC_t,protocole);
    
    save( fullfile( mytrialdir, 'res_V_lin_rel_t.mat'), 'res_V_lin_rel_t' ) ;
    save( fullfile( mytrialdir, 'res_V_ang_rel_t.mat'), 'res_V_ang_rel_t' ) ;
    disp('... . OK vitesses relatives lin et ang des rep�res segments');
end


    
    

%                     % **** CDG global dans le repere fauteuil
%                     if ~isempty( find( strcmp(lst_variables, 'res_cdg_Rfaut'), 1))
%                         disp('... CDG mod�le sujet et frm dans Rfauteuil');
%                         % ajout jba 2/1/2012
%                         load('JsRo');
%                         load('M_VIC_ANAT_t');
%                         res_cdg_Rfaut.fauteuil = calc_var_sort_CDG_Rmobile(JsRo, M_VIC_ANAT_t, 'fauteuil', 0.03) ;
%                         save res_cdg_Rfaut res_cdg_Rfaut ;
%                         disp('... . CDG mod�le sujet et frm dans Rfauteuil');
%                     end
%
%                     % **** Zero moment point dans le repere fauteuil
%                     if ~isempty( find( strcmp(lst_variables, 'res_copmod_Rfaut'), 1))
%                         disp('... zero moment point mod�le sujet et frm dans Rfauteuil');
%                         % ajout jba 30/11/2011
%                         load('res_action_totale');
%                         load('M_VIC_ANAT_t');
%                         copmod_Rvic = res_action_totale.copmod;
%                         res_copmod_Rfaut.fauteuil = sortie_calc_cop_Rfaut(copmod_Rvic, M_VIC_ANAT_t) ;
%                         save res_copmod_Rfaut res_copmod_Rfaut ;
%                         disp('... . OK zero moment point mod�le sujet et frm dans Rfauteuil');
%                     end
disp('... .OK calcul des variables cin�matiques de sortie');
