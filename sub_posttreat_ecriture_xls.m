%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      ecriture des r�sultats excel
%
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - �criture excel des donn�es
%
% En entr�e:
%   - xxxxx
%
% En sortie:
%   - xxxxx
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m",
% 'Sub_posttreat_normalisation"
%
%   - toutes les variables regroup�es
%   - lst_variables_desc, lst_variables, lst_trials, lst_var_param
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%       Pr�sentation des donn�es
%
% situation         plat	plat	plat	plat	plat
% variable          res_angles	res_angles	res_angles	res_angles	res_angles
% cycle             cycleD	cycleD	cycleD	cycleD	cycleD
% articulation      coude	coude	coude	coude	coude
% axe               x	x	x	x	x
% frame/patient     P1	P2	P3	P4	P5
% 1                 10	2	12	14	26
% 2                 3	4	7	11	18
% 3                 5	6	11	17	28
% 4                 7	8	15	23	38
% 5                 17	10	27	37	64
%


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           
%               Ecriture des variables / param�tres 
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           it�ration sur les variables / param�tres
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
disp('ecriture des fichiers') ;
log_var{ilog} = ['xxxxxxxxx  ecriture des fichiers csv'] ; ilog = ilog+1;


for i_variable = 1:length(lst_var_param)
    myvariable_file = lst_var_param{i_variable} ;
    myvariable_nom = regexprep(myvariable_file,'.mat', '') ;
    
    %     chargement de la variable
    eval(['myvar_tmp=' myvariable_nom ';']);
    
    %   Initialisation des matrices r�sultats
    idx_col = 2 ;  % index colonne �criture r�initialisation avec chq variable
    compt = 1 ;  % compteur de colonne - la premi�re �tant utilis�e pour labellisation
    lst_cycl_tmp = fieldnames(myvar_tmp) ;
    for icycle = 1:length(lst_cycl_tmp)
        mycycl = lst_cycl_tmp{icycle} ;
        lst_artic_tmp = fieldnames( myvar_tmp.(mycycl) ) ;
        for iartic = 1:length(lst_artic_tmp)
            myartic = lst_artic_tmp{iartic};
            mydata = myvar_tmp.(mycycl).(myartic) ;   % rappel : mydata de type nlignes* naxes *
            
            compt = compt + size(mydata,2)*size(mydata,3) ;
        end
    end
    
    mat_labels = cell(6,compt) ; % matrice des libell�s � �crire sur excel
    mat_labels(1:7,1) = {'Situation'; 'Variable'; 'Cycle'; 'Articulation'; 'Axe'; 'Frame/Fichier'; 'Descriptif complet'} ;
    mat_result = zeros(size(mydata,1),compt); % matrice des r�sultats num�riques
    mat_result(1:end, 1) = 1:size(mydata,1) ;
    
    
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           it�ration sur les cycles
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    for icycle = 1:length(lst_cycl_tmp)
        mycycl = lst_cycl_tmp{icycle} ;
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %           it�ration sur les articulations
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        lst_artic_tmp = fieldnames( myvar_tmp.(mycycl) ) ;
        for iartic = 1:length(lst_artic_tmp)
            myartic = lst_artic_tmp{iartic};
            mydata = myvar_tmp.(mycycl).(myartic) ;   % rappel : mydata de type nlignes* naxes *
            
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %           it�ration sur les axes
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            lst_axes_nom_gen = {'x' 'y' 'z'} ;
            lst_axes_nom = lst_axes_nom_gen(1:size(mydata,2)) ;
            for iaxe = 1:size(mydata,2)  % it�ration sur autant d'axes que contenu dans mydata
                myaxe = lst_axes_nom{iaxe} ;
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %           it�ration sur les essais / trials
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                for itrial = 1:length(lst_trials)
                    mytrial = lst_trials{itrial} ;
                    mat_labels(1:7,idx_col) = {mysituation; myvariable_nom; mycycl ; myartic; myaxe; mytrial; [mysituation, '/',  myvariable_nom, '/', mycycl ,'/',  myartic,'/',  myaxe,'/',  mytrial] } ;
                    mat_result(1:size(mydata,1),idx_col) = mydata(:,iaxe,itrial) ;
                    
                    idx_col = idx_col + 1 ;   % incr�ment de la colonne
                end     % itrial
            end         % iaxe
        end     % iartic
    end         % icycle
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %           ecriture
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % fichier de sortie
    myfile_cible_tmp = fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.csv']) ;
    
    % format des donn�es : csv s�par�s par ';', mettre '\t' pour tabulation
    format_label_tmp= [repmat('%s;', 1, size(mat_labels,2) ) , '\r\n' ] ;
    format_result_tmp = [repmat('%f;', 1, size(mat_result,2) ) , '\r\n' ] ;
    
    % ouverture du fichier cible
    fid = fopen(myfile_cible_tmp, 'w') ;
    % ecriture des libell�s (boucle n�cessaire car les cellules ne peuvent
    % �tre �crites que ligne par ligne
    for iline = 1:size(mat_labels,1)
        fprintf(fid, format_label_tmp, mat_labels{iline,:}) ;
    end
    %       ecriture des matrices (ligne par ligne)
    for iline = 1:size(mat_result, 1)
        fprintf(fid, format_result_tmp, mat_result(iline,:) ) ;
    end
%     fprintf(fid, format_result_tmp, mat_result) ;
%     fprintf(fid, format_result_tmp, mat_result) ;
    
    %       fermeture du fichier
    fclose(fid) ;
    
    
    
    
    %     % �criture excel
    %     myfile_cible_tmp = fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.dat']) ;
    % csvwrite(myfile_cible_tmp, mat_labels)
    %     csvwrite(myfile_cible_tmp, mat_result )
    %
    %
    %     save(fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.csv']), 'mat_result_cell' , '-ascii')
    %     xlswrite(fullfile(rep_resultats, [mysituation, '.xlsx']), mat_labels, myvariable_nom, 'A1') ;
    %     xlswrite(fullfile(rep_resultats, [mysituation, '.xlsx']), mat_result, myvariable_nom, 'A7') ;
    
end         % ivariable


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           
%               Ecriture des variables / param�tres MOYENS
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           it�ration sur les variables / param�tres MOYENS
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
disp('ecriture des fichiers moyenne') ;

for i_variable = 1:length(lst_var_param_moy)
    myvariable_file = lst_var_param_moy{i_variable} ;
    myvariable_nom = regexprep(myvariable_file,'.mat', '') ;
    
    %     chargement de la variable
    eval(['myvar_tmp=' myvariable_nom ';']);
    
    % xxxxxxxxxxxxxxxxxxxxxxx 
    %   Initialisation des matrices r�sultats (gain de temps de calcul)
    
    idx_col = 2 ;  % index colonne �criture r�initialisation avec chq variable
    compt = 1 ;  % compteur de colonne - la premi�re �tant utilis�e pour labellisation
    lst_cycl_tmp = fieldnames(myvar_tmp) ;
    
    for icycle = 1:length(lst_cycl_tmp)
        mycycl = lst_cycl_tmp{icycle} ;
        lst_artic_tmp = fieldnames( myvar_tmp.(mycycl) ) ;
        for iartic = 1:length(lst_artic_tmp)
            myartic = lst_artic_tmp{iartic};
            mydata = myvar_tmp.(mycycl).(myartic) ;   % rappel : mydata de type nlignes* naxes *
            
            compt = compt + size(mydata,2)*size(mydata,3) ;
        end
    end
    
    mat_labels = cell(6,compt) ; % matrice des libell�s � �crire sur excel
    mat_labels(1:7,1) = {'Situation'; 'Variable'; 'Cycle'; 'Articulation'; 'Axe'; 'Frame/Fichier'; 'Descriptif complet' } ;
    mat_result = zeros(size(mydata,1),compt); % matrice des r�sultats num�riques
    mat_result(1:end, 1) = 1:size(mydata,1) ;
    
    % xxxxxxxxxxxxxxxxxxxxxxx   fin d'initialisation des matrices
    
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           it�ration sur les cycles
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    for icycle = 1:length(lst_cycl_tmp)
        mycycl = lst_cycl_tmp{icycle} ;
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %           it�ration sur les articulations
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        lst_artic_tmp = fieldnames( myvar_tmp.(mycycl) ) ;
        for iartic = 1:length(lst_artic_tmp)
            myartic = lst_artic_tmp{iartic};
            mydata = myvar_tmp.(mycycl).(myartic) ;   % rappel : mydata de type nlignes* naxes *
            
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %           it�ration sur les axes
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            lst_axes_nom_gen = {'x' 'y' 'z'} ;
            lst_axes_nom = lst_axes_nom_gen(1:size(mydata,2)) ;
            for iaxe = 1:size(mydata,2)  % it�ration sur autant d'axes que contenu dans mydata
                myaxe = lst_axes_nom{iaxe} ;
                
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %           3 r�sultats � chq fois : donn�es moyenne, moy-SD, moy+SD
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                lst_res_tmp = {'MOY' 'MOY+SD' 'MOY-SD'} ;
                for ires = 1:3
                    myres = lst_res_tmp{ires} ;
                    mat_labels(1:7,idx_col) = {mysituation; myvariable_nom; mycycl ; myartic; myaxe; myres ;  [mysituation, '/',  myvariable_nom, '/', mycycl ,'/',  myartic,'/',  myaxe,'/',  myres] } ;
                    mat_result(1:size(mydata,1),idx_col) = mydata(:,iaxe,ires) ;
                    
                    idx_col = idx_col + 1 ;   % incr�ment de la colonne
                end     % iresult
            end         % iaxe
        end     % iartic
    end         % icycle
    
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %           ecriture
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % fichier de sortie
    myfile_cible_tmp = fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.csv']) ;
    
    % format des donn�es : csv s�par�s par ';', mettre '\t' pour tabulation
    format_label_tmp= [repmat('%s;', 1, size(mat_labels,2) ) , '\r\n' ] ;
    format_result_tmp = [repmat('%f;', 1, size(mat_result,2) ) , '\r\n' ] ;
    
    % ouverture du fichier cible
    fid = fopen(myfile_cible_tmp, 'w') ;
    % ecriture des libell�s (boucle n�cessaire car les cellules ne peuvent
    % �tre �crites que ligne par ligne
    for iline = 1:size(mat_labels,1)
        fprintf(fid, format_label_tmp, mat_labels{iline,:}) ;
    end
    %       ecriture des matrices
    for iline = 1:size(mat_result,1)
        fprintf(fid, format_result_tmp, mat_result(iline, :) ) ;
    end
    %       fermeture du fichier
    fclose(fid) ;
    
    
    
    
    %     % �criture excel
    %     myfile_cible_tmp = fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.dat']) ;
    % csvwrite(myfile_cible_tmp, mat_labels)
    %     csvwrite(myfile_cible_tmp, mat_result )
    %
    %
    %     save(fullfile(rep_resultats, [mysituation, '_' myvariable_nom '.csv']), 'mat_result_cell' , '-ascii')
    %     xlswrite(fullfile(rep_resultats, [mysituation, '.xlsx']), mat_labels, myvariable_nom, 'A1') ;
    %     xlswrite(fullfile(rep_resultats, [mysituation, '.xlsx']), mat_result, myvariable_nom, 'A7') ;
    
end         % ivariable


