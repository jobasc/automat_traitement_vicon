%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calculs cin�matiques
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Lecture fichiers C3d cin�matiques
%   - cr�ation fichier kinematic_data.mat
%   - cr�ation des rep�res segments
%   - solidification, liaisons, filtrage, interpolation
%   - calcul du rep�re salle
%
% En entr�e:
%   - i_situation, i_trial: situation et trial en cours
%   - c3d cin�matiques
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   - rep_result_cin : r�pertoire des r�sultats cin�matiques
%   - lst_situations: liste des r�pertoires situations
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat
% R_ANAT_SALLE = MP_Rsalle_Ranat % Matrice de passage du rep�re salle vers le rep�re anatomique

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%


% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               calcul des reperes techniques pour chaque acquistion
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



disp(['Calcul cin�matiques pour : ' mytrial]) ;


log_var{ilog} = ['... Calcul variables cin�matiques du fichier : ' mytrial] ; ilog = ilog+1;

% D�finition des points et rep�res Ancillaires � chaque instant dans le rep�re VICON
[Panc_Rvic_t, MP_Rvic_Ranc_t] = cinematique_ancillaire_rigid( kinematic_data.(mytrial), protocole, Panc_Rvic);



P_ANC_VIC_t = Panc_Rvic_t ;  % points de l'ancillaire dans le rep�re vicon
R_ANC_VIC_t = MP_Rvic_Ranc_t ; % Matrice de passage du rep�re ancillaire vers le rep�re vicon tq P_Rvic = MP_Rvic_Rac * P_Ranc

save( fullfile(mytrialdir, 'P_ANC_VIC_t.mat'), 'P_ANC_VIC_t' ) ;
save( fullfile(mytrialdir, 'R_ANC_VIC_t.mat'), 'R_ANC_VIC_t' ) ;
disp('...sauvegarde des fichiers de passage rep�re technique/repere anatomique ok');
log_var{ilog} = ['...sauvegarde des fichiers de passage rep�re technique/repere anatomique ok'] ; ilog = ilog+1;
%     end   % itrial
% end  % i_situation

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               Calcul des points et matrices de passage
%               avant filtre, solidification, etc.
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

disp('...D�but de fonction calcul des variables cin�matiques');


%D�finition des points et rep�res Anatomique � chaque instant dans
%le rep�re VICON - utilisation des ancillaires
disp('... ... points et rep�re dans le repere vicon');
[MP_Rvic_Ranat_t, Panat_Rvic_t] = cinematique_anatomique(kinematic_data.(mytrial), MP_Rvic_Ranc_t, MP_Ranat_Ranc, protocole);

% On calcule les points anatomiques � chaque aquisition t dans le rep�re VICON
%   grace au rep�re segmentaire et l'acquisition statique de r�f�rence
disp('... ... points anatomiques a chaque instant');
[Panat_Rvic_t] = calc_Panat_t_with_MP(MP_Rvic_Ranat, MP_Rvic_Ranat_t, Panat_Rvic, Panat_Rvic_t);


% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               Rigidification, liaisons entre segments,
%               filtres,...
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
[Panat_Rvic_t, MP_Rvic_Ranat_t] = traitement_cinematique(Panat_Rvic_t, P_ANAT_VIC, protocole, MP_Rvic_Ranat_t);

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               remplissage structure points3D
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
points3D_t = ANAT_T_2_P3D(protocole, Panat_Rvic_t, MP_Rvic_Ranat_t);


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               sauvegarde des r�sultats
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

R_ANAT_VIC_t = MP_Rvic_Ranat_t ;
P_ANAT_VIC_t = Panat_Rvic_t ;

save( fullfile(mytrialdir, 'R_ANAT_VIC_t.mat'), 'R_ANAT_VIC_t' ) ;
save( fullfile(mytrialdir, 'P_ANAT_VIC_t.mat'), 'P_ANAT_VIC_t' ) ;
save( fullfile(mytrialdir, 'points3D_t.mat'), 'points3D_t' ) ;


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                Prise en compte de l'orientation du mouvement
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

if isfield(protocole,'ORIENTATION_MOUVEMENT')
    if ~isempty(protocole.ORIENTATION_MOUVEMENT)
        disp('... prise en compte de l orientation du mouvement');
        seg_orient = fieldnames(protocole.ORIENTATION_MOUVEMENT);
        fonction = protocole.ORIENTATION_MOUVEMENT.(seg_orient{1});
        [MP_Rvic_Rsalle, MP_Rvic_Rsalle_t] = feval( fonction{1}, MP_Rvic_Ranat, MP_Rvic_Ranat_t, seg_orient{1} ) ;
        %                 eval(['[R_SALLE_VIC,R_SALLE_VIC_t]=' fonction{1} '(MP_Rvic_Ranat,MP_Rvic_Ranat_t,seg_orient{1});' ]);
    end;
else
    MP_Rvic_Rsalle = eye(4);
    list_seg = fieldnames(MP_Rvic_Ranat_t);
    duration_time = size(MP_Rvic_Ranat_t.(list_seg{1}),3) ;
    MP_Rvic_Rsalle_t = repmat( eye(4), [1, 1, duration_time] );
end

%       sauvegarde
R_SALLE_VIC = MP_Rvic_Rsalle ;
R_SALLE_VIC_t = MP_Rvic_Rsalle_t ;

save( fullfile(rep_result_stat, 'R_SALLE_VIC.mat'), 'R_SALLE_VIC' ) ;
save( fullfile(mytrialdir, 'R_SALLE_VIC_t.mat'), 'R_SALLE_VIC_t' ) ;


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                    LEGNANI:
%       Matrices de passage, matrices vitesse, matrices acceleration
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
disp( ['... calcul matrices cinematiques legnani : '  mytrial] );
log_var{ilog} = ['... calcul matrices cinematiques legnani : ' mytrial] ; ilog = ilog+1;

[M_VIC_ANAT_t, W_ANAT_VIC_t, H_ANAT_VIC_t] = ...
    matrice_cinematique_legnani(MP_Rvic_Ranat_t, list_segments, protocole);

save(fullfile(mytrialdir, 'M_VIC_ANAT_t'),'M_VIC_ANAT_t');    % (M_VIC_ANAT_t en [m]  )
save(fullfile(mytrialdir, 'W_ANAT_VIC_t'),'W_ANAT_VIC_t');    % (W_ANAT_VIC_t en [m/s])
save(fullfile(mytrialdir, 'H_ANAT_VIC_t'),'H_ANAT_VIC_t');    % (H_ANAT_VIC_t [m/s�]  )






