%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Cr�ation du mod�le (volume, inertie)
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - cr�ation du mod�le
%   - utilisation de De leva
%
% En entr�e:
%   - xxx
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - model_file :  fichier mod�le
%   - MP_Rvic_Ranat : matrice de passage du Rep�re anatomique vers Repere vicon
%   - patient_data : donn�es patient, notamment genre M ou F
%   - F_ref: poids du patient, enregistr� � la prise de r�f�rence
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

%           Lecture fichier mod�le (nommodele.mod)
model_prot = lire_fichier_prot( model_file ) ;

% initialisation des variables importantes
additional_data.MP_Rvic_Ranat = MP_Rvic_Ranat ;
additional_data.model_perso = struct ;  % initialisation de model_perso pour ne pas tenter de charger un fichier
additional_data.patient_data = patient_data ;
additional_data.F_ref = F_ref ;

%       Calcul des param�tres inertiels
model_perso = inertial_parameters( patient_courant, model_prot, additional_data ) ;




