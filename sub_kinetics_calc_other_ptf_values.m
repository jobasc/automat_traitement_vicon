%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calculs des efforts externes - �l�ments
%                      compl�mentaires
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Calcul des efforts externes et �l�ments compl�mentaires (cop, actions meca, etc.)
%
% En entr�e:
%   -
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       modifications
%
% bascou 2016 05 10 cr�ation
%


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

disp('plateformes: calcul de r�sultats suppl�mentaires (fct sub_kinetics_calc_other_ptf_values)') ;

%%xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           calcul du poids de reference du sujet a partir des efforts plateforme
if ~exist( 'F_ref', 'var')
    Fref = 9.81;
    errordlg('attention donnees non normalisees par la masse du sujet');
end

% calcul du poids de reference du sujet a partir des efforts
% plateforme pour la normalisation des donn�es
[effort_ref,~] = ref_force_plate(F_ref);


if isfield(protocole,'CYCLE')
    %     expression des efforts et moments dans le rep�re salle
    sides = fieldnames(protocole.CYCLE);
    [res_actions_meca, res_effort, res_moment, res_cop] = var_sortie_plateforme(actions_meca, R_SALLE_VIC_t, sides, effort_ref);
    
    save(  fullfile( mytrialdir, 'res_actions_meca'), 'res_actions_meca');
    save(  fullfile( mytrialdir, 'res_effort'), 'res_effort');
    save(  fullfile( mytrialdir, 'res_moment'), 'res_moment');
    save(  fullfile( mytrialdir, 'res_cop'), 'res_cop');
    
else
    disp('attention pas cycle d�fini dans le .prot');
end;
