

% Script
%           sub_Mresults_calc_param_marche
%
% Objectif:
%   -calcul des parametres g�n�raux de la marche pour chaque marche du sujet
%
% En entr�e:
%   - cycle, points3D_t ,marqueur_vitesse ,freq, res_variable.cycle, kinematic_data, res_var_angle_t
%   - freq
%   - protocole
%                 load('cycle');
%                 load('points3D_t');
%                 load('res_var_angle_t');

%                 freq = str2num(protocole.FREQUENCE.cam{1});
if isfield(protocole,'PARAMETRES')
    marqueur_vitesse = protocole.PARAMETRES.vitesse{1};
else
    marqueur_vitesse='C7';
end;
frequence_acqui = str2double(protocole.FREQUENCE.cam{1}) ;

param_marche = calcule_param_generaux(cycle, points3D_t ,marqueur_vitesse ,frequence_acqui, res_variable.cycle, kinematic_data, res_var_angle_t);

save( fullfile( mytrialdir, 'param_marche.mat'), 'param_marche' );
