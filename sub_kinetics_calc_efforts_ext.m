%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calculs des efforts externes
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Calcul des efforts externes (ex: ptf de force)
%
% En entr�e:
%   -
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       modifications
%
% bascou 2015 11 04 cr�ation
%

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

disp('...Calcul des efforts externes') ;
[F, actions_meca] = mechanical_actions(mytrial, kinematic_data);

save( fullfile( mytrialdir, 'F.mat'), 'F');
save( fullfile( mytrialdir, 'actions_meca.mat'), 'actions_meca') ;

disp('...Fin de calcul des efforts externes') ;