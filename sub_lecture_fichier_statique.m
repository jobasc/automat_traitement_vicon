%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           Lecture du fichier statique c3d
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
% Objectif:
%   - lecture du fichier statique c3d
%   - stockage dans static data
%   - sauvegarde dans fichiers_reference\static_data.mat
%
% En entr�e:
%   - nom du fichier statique.c3d
%
% En sortie:
%   - variable "static_data"
%   - fichier static_data.mat
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - rep_result_stat : r�pertoire analyse\patientxxx\traitement\resultats_stat
%   - stat_file : cell contenant le chemin des fichiers C3d statique
%   (normalement un seul)


log_var{ilog} = ['xxxxxxxxx  lecture fichier statique'] ; ilog = ilog+1;
% lecture des donnees
for ifile = 1:length(stat_file)
    [rep_stat, stat_file_nom, stat_file_suff] = fileparts( stat_file{ifile} ) ;
    static_data.( stat_file_nom ) = lire_donnees_c3d( stat_file{ifile} );
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %       Nettoyage des noms de marqueurs
    %
    %   (Enlever le nom du mod�le, qui parfois s'ins�re dans le nom
    %   marqueur)
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    lst_mkrs_tmp = static_data.( stat_file_nom ).noms;
%     [ model_name, newlabels ] = search_modelname_from_mkrlst( lst_mkrs_tmp ) ;
    [newlabels] = sub_clean_mkrs_names(lst_mkrs_tmp ) ; 
    
    if ~isempty( find( strcmp( lst_mkrs_tmp, newlabels ) == 0 ) )
        disp( 'certains libell�s marqueurs ont �t� retouch�s' );
        static_data.( stat_file_nom ).noms = newlabels ;
    end
    
    %% passage des coordonn�es en m!! et des moments en Nm
    static_data.( stat_file_nom ).coord = static_data.( stat_file_nom ).coord/1000;
    for actmec_sel=1:size(static_data.( stat_file_nom ).actmec,2)/12
        static_data.( stat_file_nom ).actmec(:,1+12*(actmec_sel-1):6+12*(actmec_sel-1)) = static_data.( stat_file_nom ).actmec(:,1+12*(actmec_sel-1):6+12*(actmec_sel-1))/1000;
        static_data.( stat_file_nom ).actmec(:,10+12*(actmec_sel-1):12+12*(actmec_sel-1)) = static_data.( stat_file_nom ).actmec(:,10+12*(actmec_sel-1):12+12*(actmec_sel-1))/1000;
    end;
end

static_data_file = fullfile( rep_result_stat, 'static_data.mat') ;
save( static_data_file, 'static_data' );

log_var{ilog} = ['... fin de lecture du fichier statique'] ; ilog = ilog+1;

