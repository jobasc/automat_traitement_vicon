%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Dynamique - calcul des r�sultats
%
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
% Objectif:
%   - Calcul des r�sultats de la dynamique
%
% En entr�e:
%   - Phi_Ranat: matrices d'efforts en rep�re anatomique
%   - res_effort
%   - res_moment
%   - JsRo : pseudo inertie dans rep�re R0
%   - W_ANAT_VIC_t
%   - V_CDG_VIC_t
%   - actions_meca: actions meca plateformes
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%       modifications
% bascou 2015 11 04 : cr�ation
% bascou 2016 06 16 : d�placement calcul du poids de r�f�rence dans
% sub_kinetics_calc_other_ptf_values
%
%

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% liste des segments du mod�le perso
if avecinertie
    segment_modele = fieldnames(model_perso);
end


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%      Efforts et moments articulaires
%
%   - n�cessite Phi_Ranat et Phi_Ro
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
if sum( strcmp(lst_variables, 'res_effort')) > 0   ||   sum( strcmp(lst_variables, 'res_moment') ) > 0
    disp('... calcul des efforts et moments articulaires');
    
    % transformation du formalisme de legnani en efforts/moments
    [res_effort_articulaire, res_moment_articulaire] = legnani2resmoment(Phi_Ranat, effort_ref);
    
    try
        % xxxxxxx  xx  xxxxxxx  xx  xxxxxxx  xx  xxxxxxx  xx  xxxxxxx  xx
        
        res_effort = mergestruct(res_effort, res_effort_articulaire);
        res_moment = mergestruct(res_moment, res_moment_articulaire);
    catch
        [res_effort,res_moment] = legnani2resmoment(Phi_Ro, effort_ref);
        %                         [res_effort_pes,res_moment_pes]=legnani2resmoment(Phi_PES,effort_ref);
        res_effort = mergestruct(res_effort, res_effort_articulaire);
        res_moment = mergestruct(res_moment, res_moment_articulaire);
    end;
    
    save(  fullfile( mytrialdir, 'res_effort.mat'), 'res_effort' );
    save(  fullfile( mytrialdir, 'res_moment.mat'), 'res_moment' );
    
    disp('... .OK calcul des efforts et moments articulaires');
end;

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%     	Energie cin�tique / potentielle
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
if sum( strcmp(lst_variables, 'res_Ec_t')) > 0   &&  sum( strcmp(lst_variables, 'res_Ep_t') ) > 0
    
    disp('... calcul des energies cinetiques et potentielles');
    
    [res_Qte_MVT_lin_t, res_Qte_MVT_ang_t, res_Ec_t, res_Ep_t] = calcul_cinetique_energie_legnani(Qte_MVT_t_legnani, W_ANAT_VIC_t, JsRo, effort_ref) ;
    
    save(   fullfile( mytrialdir, 'res_Qte_MVT_lin_t.mat')  , 'res_Qte_MVT_lin_t') ;
    save(   fullfile( mytrialdir, 'res_Qte_MVT_ang_t.mat')  , 'res_Qte_MVT_ang_t') ;
    save(   fullfile( mytrialdir, 'res_Ec_t.mat'    )       , 'res_Ec_t') ;
    save(   fullfile( mytrialdir, 'res_Ep_t.mat'    )       , 'res_Ep_t') ;
    disp('... .OK calcul des efforts et moments articulaires');
end;

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%     	Puissance cin�tique et articulaire
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
if ~isempty( find( strcmp(lst_variables, 'res_puissance'), 1))
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           calcul de puissance cinetique du centre de gravite
    try
        [res_puissance_cdg, m, V_CDG_VIC_t] = puissance_cdg(V_CDG_VIC_t, JsRo, actions_meca, effort_ref);
        res_Ec_t.GTOTAL=[zeros(length(V_CDG_VIC_t.total),1),zeros(length(V_CDG_VIC_t.total),1),0.5*dot(V_CDG_VIC_t.total,V_CDG_VIC_t.total,2)*m.total]/(effort_ref/9.81);
        save( fullfile( mytrialdir, 'res_Ec_t.mat'), 'res_Ec_t') ;
    catch
        res_puissance_cdg = struct ;
        disp('Puissance cin�tique impossible � calculer') ;
    end;
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           puissance articulaire           (moment * vangulaire articulation)
    try
        [res_puissance_arti_rot, res_puissance_arti_trans, res_puissance_arti_tot] = ...
            puissance_articulaire(W_ANAT_VIC_t, Phi_Ranat, M_VIC_ANAT_t, protocole.ARTICULATIONS, protocole.CENTRES_ARTICULAIRES, effort_ref);
    catch
        res_puissance_arti_rot = struct;
        disp('il manque des donnees d entree pour le calcul de la puissance');
    end;
    
    % merge des puissances cin�tique et articulaire
    res_puissance = mergestruct(res_puissance_cdg,res_puissance_arti_rot);
    
    % sauvegarde
    save( fullfile( mytrialdir, 'res_puissance.mat'),'res_puissance');
    
    
end;  % ~isempty( find( strcmp(lst_variables, 'res_puissance'), 1))

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               Position des centres de gravite
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

if ~isempty( find( strcmp(lst_variables, 'res_CDG_SALLE_t'), 1)) && exist('JsRo')
    disp('... calcul des centres de gravite');
    try
        %     load( fullfile( mytrialdir, 'CDG_VIC_t.mat') ) ;
        
        
        res_CDG_SALLE_t = P3D_VIC_2_SALLE(CDG_VIC_t, R_SALLE_VIC_t);
        
        
        save( fullfile( mytrialdir, 'res_CDG_SALLE_t.mat'), 'res_CDG_SALLE_t' ) ;
    catch
        disp('calcul du CDG dans rep�re salle NOK') ;
    end
    try
        %         load( fullfile( mytrialdir, 'V_CDG_VIC_t.mat') ) ;
        res_V_CDG_SALLE_t = P3D_VIC_2_SALLE(V_CDG_VIC_t, R_SALLE_VIC_t);
        save( fullfile( mytrialdir, 'res_V_CDG_SALLE_t.mat'), 'res_V_CDG_SALLE_t' ) ;
    catch
        disp( 'V_CDG_VIC_t.mat absent, impossible de calculer vitesse CDG dans Rsalle') ;
    end;
    
end  % if variable res_cdg_salle existante




