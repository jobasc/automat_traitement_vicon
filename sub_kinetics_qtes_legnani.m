%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%          Quantit�s dynamiques de Legnani
%
%                   ( si mod�le calcul�)
%
%                       SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - pseudo inertie, qtes acceleration, qte mvt, cdg avec Legnani
%
% En entr�e:
%   - model_perso
%   -
%   - points3D_t
%
% En sortie:
%   - JsRo      : pseudo inertie legnani dans R0
%   - JsRs      : pseudo inertie legnani dans rep�res segments
%   - CDG_VIC_t     : position centre de gravit�
%   - V_CDG_VIC_t   : vitesse de centre de gravit�
%   - Phi_PES           : poids associ� � chq segment
%   - A_ANAT_VIC_t      : qt� acc�l�ration
%   - Qte_MVT_t_legnani : qte mouvement
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "sub_kinetics.m"
%
%   - MP_Rvic_Ranat_t
%   - M_VIC_ANAT_t: matrice position legnani
%   - W_VIC_ANAT_t: matrice vitesse legnani
%   - H_VIC_ANAT_t: matrice acc�l�ration legnani
%   - list_segments
%   - protocole
%   - model_perso
%   - mytrialdir
%   - points3D_t
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

disp(['... Calcul quantit�s dynamique legnani : ' mytrial]) ;
log_var{ilog} = ['... Calcul quantit�s dynamique legnani du fichier : ' mytrial] ; ilog = ilog+1;

warning off ;

if avecinertie
    try
        
        % matrices de pseudo inertie
        segment_modele = fieldnames(model_perso);
        [JsRo,JsRs] = calcul_matrice_pseudo_inertie(M_VIC_ANAT_t, model_perso, segment_modele);
        save(fullfile(mytrialdir, 'JsRo.mat'), 'JsRo' );
        save(fullfile(mytrialdir, 'JsRs.mat'), 'JsRs' );
     
        %             %Matrice pesanteur
        %             Hg = zeros(4,4);
        %             Hg(3,4) = - 9.81;
        
        % utilisation des fonctions de legnani pour le calcul des quantit�s dynamiques
        [Phi_PES, A_ANAT_VIC_t, Qte_MVT_t_legnani] = legnani_dyn_iseg(JsRo, H_ANAT_VIC_t, W_ANAT_VIC_t, segment_modele);
        
        save(fullfile( mytrialdir,'A_ANAT_VIC_t.mat' ), 'A_ANAT_VIC_t' );
        save(fullfile( mytrialdir,'Phi_PES.mat' ), 'Phi_PES' );
        save(fullfile( mytrialdir,'Qte_MVT_t_legnani.mat' ), 'Qte_MVT_t_legnani' );
            catch
        disp(' xxxxxxxxxxxxxx  ALERT   pb in inertia calculation legnani    xxxxxxxxxxxxxxxxxxx') ;
    end;
    
    % calculs de centre de gravit�
    try
        
        %           Position Centre de gravit� dans rep�re vicon
        CDG_VIC_t = calcul_cdg_t(JsRo);
        CDG_VIC_t.total=( points3D_t.EPSD + points3D_t.EPSG + points3D_t.EASD + points3D_t.EASG )/4;
        save( fullfile(mytrialdir, 'CDG_VIC_t.mat'),'CDG_VIC_t');
        
        %           vitesse Centre de gravit� dans rep�re vicon
        V_CDG_VIC_t = calcule_V_points_segments(W_ANAT_VIC_t, CDG_VIC_t);
        V_CDG_VIC_t.total = derive_MH( permute( CDG_VIC_t.total, [3,2,1] ), 100);
        V_CDG_VIC_t.total = [squeeze(V_CDG_VIC_t.total)]';
        save( fullfile(mytrialdir, 'V_CDG_VIC_t.mat') ,  'V_CDG_VIC_t' );
        
    catch
        disp(' xxxxxxxxxxxxxx  ALERT   pb in cdg calculation legnani    xxxxxxxxxxxxxxxxxxx') ;
    end;
end

disp(['xxx Fin Calcul quantit�s dynamique legnani du fichier : ' mytrial]) ;