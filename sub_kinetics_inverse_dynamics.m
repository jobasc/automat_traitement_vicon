%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calculs dynamiques
%
%       A_ANAT_VIC, PHI_PES, Qte_MVT_t_legnani
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Calcul des variables dynamiques
%
% En entr�e:
%   -
%
% En sortie:
%   - Phi_PES: pseudo matrice de pesanteur
%   - A_ANAT_VIC_t: matrice quantit� d'acc�l�ration
%   - Qte_MVT_t_legnani: matrice quantit� de mouvement
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   -R_ANAT_VIC_t  P_ANAT_VIC_t A_ANAT_VIC_t Phi_PES 
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat



%
%     disp(' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' ) ;
%         disp('       Calcul du torseur d effort par dynamique inverse    ');
%         disp(' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' ) ;


Phi_Ranat=[];
Phi_Ro=[];


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% 
%           Remplissage structure des efforts Phi_Ro 
%
%   - formalisme de Legnani
%   - Avec les fonctions et leurs arguments list�s dans le protocole
%   - Si non rempli, utilisation d'une fonction par d�faut
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

disp('... chargement des efforts externes');
%           Remplissage structure Phi_Ro avec efforts externes 
[Phi_Ro] = effort_externe(protocole, mytrialdir);

% **** calcul des cha�nes de dynamique inverse
disp('... Traitement des chaines de dynamique inverse')

if ~avecinertie
    A_ANAT_VIC_t = struct ;
    Phi_PES = struct;
end

% efforts articulaires dans rep�re R0 et rep�res anatomiques - par dynamique inverse
[Phi_Ro, Phi_Ranat] = dynamique_inverse_legnani(Phi_Ro, A_ANAT_VIC_t, Phi_PES, R_ANAT_VIC_t, P_ANAT_VIC_t, protocole, avecinertie);

disp( '... fin de calcul des chaines de dynamique inverse');
disp( ' ***********************' );


% sauvegarde
save( fullfile( mytrialdir, 'Phi_Ranat.mat'), 'Phi_Ranat' ) ;
save( fullfile( mytrialdir, 'Phi_Ro.mat'), 'Phi_Ro' ) ;

