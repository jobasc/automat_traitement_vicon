% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           Cr�ation des r�pertoires de travail (analyse, resultat, etc.)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

%   Extraction du r�pertoire de la base de donn�es
[glob_rep, raw_base_rep] = fileparts(rep_prot) ;


% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%       cr�ation des r�pertoires analyse
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% xxxxxxxxxxxxxxx           Repertoire Analyse
[glob_rep, raw_base_rep] = fileparts(rep_prot) ;
rep_base_analyse = fullfile(glob_rep, [raw_base_rep,'_Analyse']) ; % Au m�me niveau que les rep situations
log_var{ilog} = ['rep_analyse: ', rep_base_analyse] ; ilog = ilog+1;

if ~exist( rep_base_analyse, 'dir')
    mkdir( rep_base_analyse) ;
    log_var{ilog} = ['...Cr�ation du r�pertoire base analyse'] ; ilog = ilog+1;
end

% xxxxxxxxxxxxxxx           Repertoire Classification
rep_analyse = fullfile(rep_base_analyse, classif_nom ) ; % Au m�me niveau que les rep classifications

% xxxxxxxxxxxxxxx          Repertoire patient_cible, contient:  rep_mouvement,  r�sultat_xls
rep_patient_cible = fullfile(rep_analyse, patient_nom) ;
log_var{ilog} = ['rep_patient_cible: ', rep_patient_cible] ; ilog = ilog+1;
if ~exist( rep_patient_cible,  'dir')
    mkdir( rep_patient_cible) ;
    log_var{ilog} = ['...Cr�ation du r�pertoire rep_patient_cible'] ; ilog = ilog+1;
end


% xxxxxxxxxxxxxxx           Repertoire Resultats contient les r�sultats excel exploitables par l'utilisateur sans matlab
rep_resultats = fullfile(rep_patient_cible, 'recap_resultats_xls') ;
log_var{ilog} = ['rep_resultats: ', rep_resultats] ; ilog = ilog+1;
if ~exist( rep_resultats,  'dir')
    mkdir( rep_resultats) ;
    log_var{ilog} = ['...Cr�ation du r�pertoire rep_resultats'] ; ilog = ilog+1;
end

% xxxxxxxxxxxxxxx          r�pertoire r�sultats stat : position des ancillaires, r�f�rence, etc.
% enregistrement des donnees
patient_courant = rep_patient_cible ;
rep_result_stat = fullfile( patient_courant, 'resultats_stat') ;
if ~exist( rep_result_stat ,'dir');
    mkdir( rep_result_stat ) ;
end



% xxxxxxxxxxxxxxx          situations cin�matiques
% enregistrement des donnees

% for i_situation = 1:size(lst_situations,2)
%     mysituation = lst_situations{i_situation} ;
%     mysit_dir = fullfile( patient_courant, mysituation) ;
%     if ~exist( mysit_dir ,'dir');
%         mkdir( mysit_dir ) ;
%     end
%     if ~exist( fullfile( mysit_dir, 'resultats_cinemat') ,'dir');
%         mkdir( fullfile( mysit_dir, 'resultats_cinemat')  ) ;
%     end
%     if ~exist( fullfile( mysit_dir, 'fichiers_cinemat') ,'dir');
%         mkdir( fullfile( mysit_dir, 'fichiers_cinemat')  ) ;
%     end
% %     % xxxxxxxxxxxxxxx          r�pertoire r�sultats cinemat : angles, vitesses, etc.
% %     rep_result_cin = fullfile( mysit_dir, 'resultats_cinemat') ;
% %     if ~exist( rep_result_cin ,'dir');
% %         mkdir( rep_result_cin ) ;
% %     end
% %
%
% end
