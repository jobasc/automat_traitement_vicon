function [masse,CDG_ANAT,IG_ANAT]=calc_inerties_mod_proportionnel(segment,model_prot,points3D,sexe,masse_ref);


if strfind(segment,'D')
    seg=strrep(segment,'D','');
    side='Right';
elseif strfind(segment,'G')
    seg=strrep(segment,'G','');
    side='Left';
else
        side='Right';
    seg=segment;
end;

% Extr�mit� proximale : calcul de la moyenne des points donn�es dans le fichier .mod
vartmp=length(model_prot.(segment).extremite_proximale); Extproximale=[0,0,0]; k=0;
for iii=1:vartmp
    Extproximale=(k*Extproximale + points3D.(eval(strcat('model_prot.',(segment),'.extremite_proximale{',num2str(iii),'}'))) )/(k+1);
    k=k+1;
end
clear vartmp k

% Extr�mit� distale : calcul de la moyenne des points donn�es dans le fichier .mod
vartmp=length(model_prot.(segment).extremite_distale); Extdistale=[0,0,0]; k=0;
for iii=1:vartmp
    Extdistale=(k*Extdistale + points3D.(eval(strcat('model_prot.',(segment),'.extremite_distale{',num2str(iii),'}'))) )/(k+1);
    k=k+1;
end
clear vartmp k

switch model_prot.(segment).methode{1}
    case 'deleva'
        [masse,coeff_G,coeff_I]=paulo_de_leva(Extproximale,Extdistale,seg,masse_ref,sexe,eval(strcat('model_prot.',(segment),'.origine')));
        
        if strfind(segment,'pied')
            long_pied=norm(Extproximale-Extdistale);
            CDG_ANAT=[coeff_G,0,0];
            IG_ANAT=diag([coeff_I(2),coeff_I(1),coeff_I(3)]);
        else
            CDG_ANAT=[0,coeff_G,0];
            IG_ANAT=diag(coeff_I);
        end;
    case 'dumas'
        [masse,CDG_ANAT,IG_ANAT]=BSIP_Dumas2007(Extproximale,Extdistale,seg,side,masse_ref,sexe,eval(strcat('model_prot.',(segment),'.origine')));
        
end