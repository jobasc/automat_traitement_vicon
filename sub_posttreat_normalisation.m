%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Normalisation
%
%       A_ANAT_VIC, PHI_PES, Qte_MVT_t_legnani, rep_res_cin, lst_variables_desc
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - expression normalis�e des donn�es
%
% En entr�e:
%   - rep_res_cin, lst_variables_desc
%
% En sortie:
%   - Phi_PES: pseudo matrice de pesanteur
%   - A_ANAT_VIC_t: matrice quantit� d'acc�l�ration
%   - Qte_MVT_t_legnani: matrice quantit� de mouvement
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m",
% 'Sub_posttreat"
%
%   - lst_variables_desc, lst_variables, lst_trials
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat


       % *** liste des mouvements
        % % % movement_list = get(findobj('tag','liste_marches'),'string');
        
        % echantillonnage
        if isfield(protocole.FREQUENCE,'echant')
            echant = str2num(protocole.FREQUENCE.echant{1});
        else
            echant = 2 ;
        end
        
        % regroupement des variables et enregistrement
        for i_variable = 1:length(lst_variables)
            myvariable = lst_variables{i_variable} ;
            myvariable_nom = strrep(lst_variables{i_variable},'.mat','') ;
            
            variable_regroupee = regroupe_donnees(myvariable, lst_trials_full, lst_variables_desc, echant);
            
            if ~isempty(variable_regroupee)
                eval([myvariable_nom '= variable_regroupee;']);
                save(fullfile(rep_res_cin,  [myvariable '.mat'] ), myvariable_nom);
            end;
            
        end;
        
%         set(findobj('tag','liste_variable'),'string',liste_variable(strncmp(liste_variable,'res_',4)));
%         actualise_fenetre(patient_courant,situation);
        disp('Normalisation effectu�e') ;
        
        