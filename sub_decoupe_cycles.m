%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                      Calculs cin�matiques
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Lecture fichiers C3d cin�matiques
%   - cr�ation fichier kinematic_data.mat
%   - cr�ation des rep�res segments
%   - solidification, liaisons, filtrage, interpolation
%   - calcul du rep�re salle
%
% En entr�e:
%   - c3d cin�matiques
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   - rep_result_cin : r�pertoire des r�sultats cin�matiques
%   - lst_situations: liste des r�pertoires situations
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat
% R_SALLE_VIC = MP_Rvic_Rsalle ; % Matrice de passage du rep�re salle vers le rep�re vicon
% R_SALLE_VIC_t = MP_Rvic_Rsalle_t ; % Matrice de passage du rep�re salle vers le rep�re vicon

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               chargement des fichiers � lire
%                   cr�ation de kinematic_data
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

for i_situation = 1:size(lst_situations, 2)   %  it�ration conditions (plat, d�vers, pente etc.)
    mysituation = lst_situations{i_situation} ;
    disp( [ 'lecture et crea kinematic data pour :   ' lst_situations{i_situation} ] ) ;
    mysit_dir = fullfile( patient_courant, mysituation, 'fichiers_cinemat') ; % chemin de la situation
    tmp_lst = dir( fullfile( mysit_dir, '*.c3d' ) );    % lst des fichiers c3d du chemin
    lst_sit_c3dfiles = { tmp_lst(:).name} ;
    lst_sit_c3dfilesfull = fullfile( mysit_dir, lst_sit_c3dfiles) ;     % chemin complet des fichiers c3d
    kinematic_data = lecture_donnees_cinematique( lst_sit_c3dfilesfull );
    
    %           sauvegarde du fichier kinematic_data
    rep_res_cin = fullfile( patient_courant, mysituation, 'resultats_cinemat' ) ;
    save( fullfile( rep_res_cin, 'kinematic_data.mat'), 'kinematic_data' );
    log_var{ilog} = ['xxxxxxxxx fin lecture fichier cinematique de la situation ' mysituation] ; ilog = ilog+1;
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %               calcul des reperes techniques pour chaque acquistion
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    lst_c3d = fieldnames(kinematic_data);
    
    for ic3d = 1:length(lst_c3d)   % it�ration fichiers cin�matiques
        clear('Panc_Rvic_t', 'MP_Rvic_Ranc_t', 'Panat_Rvic_t', 'MP_Rvic_Ranat_t', 'points3D_t' ) ;
        
        
        myc3d = lst_c3d{ic3d} ;
        rep_c3d = fullfile( rep_res_cin, myc3d) ;
        if ~exist( rep_c3d, 'dir')
            mkdir( rep_c3d) ;
        end
        disp(['... Calcul variables cin�matiques du fichier : ' myc3d]) ;
        log_var{ilog} = ['... Calcul variables cin�matiques du fichier : ' myc3d] ; ilog = ilog+1;
        

        
        
        
        
        
        
        
    end     % ic3d
end     % isituation



