%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           Copie de la base dans base_scann�e
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Copie de la base initiale dans une base : base_scann�e (des modifs
%   pourront y �tre apport�es sans modifier les donn�es brutes
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire contenant les fichiers .plat des situations � traiter.
%       . Contiennent notamment la position des plateformes de force
%       . FORMAT: devers_CERAH.plat ou pente5_CERAH.plat ou pente5_IRR.plat
%   - r�pertoire de destination
%   - choix des patients � traiter
%   - choix des situations � traiter
%
% En sortie:
%   - base copi�e dans base scanned (uniquement le patient en cours)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           Cr�ation des r�pertoires de travail (analyse, resultat, etc.)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

%   Extraction du r�pertoire de la base de donn�es
[glob_rep, raw_base_rep] = fileparts(rep_prot) ;


% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%       cr�ation des r�pertoires base scann�e
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


% xxxxxxxxxxxxxxx           Repertoire Base scann�e
%       copie de la base principale, dans laquelle est fait le scanbase

rep_base_scanned = fullfile(glob_rep, [raw_base_rep,'_Scanned']) ; % Au m�me niveau que les rep situations
log_var{ilog} = ['rep_scanned: ', rep_base_scanned] ; ilog = ilog+1;

if ~exist( rep_base_scanned, 'dir')
    mkdir( rep_base_scanned) ;
    log_var{ilog} = ['...Cr�ation du r�pertoire base scann�e'] ; ilog = ilog+1;
    
    disp('...copie fichiers rep prcp dans base scann�e');
    copyfile( fullfile(rep_prot, '*.????'), rep_base_scanned) ;
    log_var{ilog} = ['...fichiers rep prcp copi�s dans base scann�e'] ; ilog = ilog+1;
end

% xxxxxxxxxxxxxxx           r�pertoire classification
rep_classif_base_scanned = fullfile(rep_base_scanned, classif_nom ) ; % Au m�me niveau que les rep classifications

if ~exist( rep_classif_base_scanned, 'dir')
    mkdir( rep_classif_base_scanned) ;
    log_var{ilog} = ['...Cr�ation du r�pertoire classif base scann�e'] ; ilog = ilog+1;
    
    disp('...copie fichiers classif dans base scann�e');
    copyfile( fullfile(rep_prot, classif_nom, '*.???'), rep_classif_base_scanned) ;
    log_var{ilog} = ['...fichiers classif copi�s dans base scann�e'] ; ilog = ilog+1;
end

% copie s�lective dans base scann�e du patient, de la session ou de l'essai
switch traitement_seluser
    case 'Patient'
        % xxxxxxxxxxxxxxx           r�pertoire patient
        rep_patient_base_scanned = fullfile(rep_base_scanned, classif_nom, patient_nom) ; % Au m�me niveau que les rep classifications
        
        if ~exist( rep_patient_base_scanned, 'dir')
            mkdir( rep_patient_base_scanned) ;
            log_var{ilog} = ['...Cr�ation du r�pertoire patient - base scann�e'] ; ilog = ilog+1;
            
            disp('...copie fichiers patients dans base scann�e');
            copyfile( fullfile(rep_prot, classif_nom, patient_nom), rep_patient_base_scanned) ;
            log_var{ilog} = ['...fichiers patients copi�s dans base scann�e'] ; ilog = ilog+1;
        end
        
    case 'Session'
        % on ne fait rien: on suppose que les fichiers du patient sont d�j�
        % scann�s et �crits dans base_scanned
        
        rep_patient_base_scanned = fullfile(rep_base_scanned, classif_nom, patient_nom) ; % Au m�me niveau que les rep classifications
        
    case 'Essai'
        % on ne fait rien: on suppose que les fichiers du patient sont d�j�
        % scann�s et �crits dans base_scanned
        rep_patient_base_scanned = fullfile(rep_base_scanned, classif_nom, patient_nom) ;
        
    otherwise
        % normalement jamais le cas
        rep_patient_base_scanned = fullfile(rep_base_scanned, classif_nom, patient_nom) ;
end
