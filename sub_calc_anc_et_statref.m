%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           Calcul des param�tres statiques
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
% Objectif:
%   - calcul des rep�res ancillaires
%   - calcul de la position statique de r�f�rence
%
% En entr�e:
%   - static_data : structure d�crivant les coord et noms des mkrs statiques
%
% En sortie:
%   -
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - static_data
%   - protocole
%
% xxxxxxxxxxxxxxxxxxxxxxxx
%       Rappel
% P_ANAT_ANC = Panat_Ranc   	% Points anatomiques dans le rep�re ancillaire
% M_ANAT_ANC = MP_Ranat_Ranc 	% Matrice de passage du rep�re ancillaire vers le rep�re anat tq P_Ranat = MP_Ranat_Ranc * P_Ranc
% ANC_GEO = Panc_Rvic			% points de l'ancillaire dans le rep�re vicon
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% R_ANAT_VIC = MP_Rvic_Ranat 	% Matrice de passage du rep�re anatomique vers le rep�re vicon tq P_Rvic = MP_Rvic_Ranat * P_Ranat

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%       Chargement/initialisation
%           - des rep�res ancillaires
%           - de la matrice de passage des rep�res anatomiques vers les ancillaires
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% % % if exist( fullfile(rep_result_stat, 'MP_Ranat_Ranc.mat'), 'file' )
% % %     load( fullfile(rep_result_stat, 'MP_Ranat_Ranc.mat') ) ;
% % % end
% % % if exist( fullfile(rep_result_stat, 'Panc_Rvic.mat'), 'file' )
% % %     load( fullfile(rep_result_stat, 'Panc_Rvic.mat') ) ;
% % % end

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%            Retro compatibilit� avec mouvement: 
%   les structures MP_Ranat_Ranc et
%   Panc_Rvic �taient appel�es M_ANAT_ANC et ANC_GEO, respectivement

if exist( fullfile(rep_result_stat, 'M_ANAT_ANC.mat'), 'file' )
    load( fullfile(rep_result_stat, 'M_ANAT_ANC.mat') ) ;
    MP_Ranat_Ranc = M_ANAT_ANC ;
else
    MP_Ranat_Ranc = struct;
end

if exist( fullfile(rep_result_stat, 'ANC_GEO.mat'), 'file' )
    load( fullfile(rep_result_stat, 'ANC_GEO.mat') ) ;
    Panc_Rvic = ANC_GEO ;
else
    Panc_Rvic = struct;
end

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           calcul des relations entre rep�res anatomiques et ancillaires
%         
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%   Calcul des matrices de passage et points dans les diff�rents rep�res
[MP_Ranat_Ranc_new, Panat_Ranc, Panc_Rvic_new] = statique_anat_anc( static_data, protocole );

% Fusion avec les valeurs �ventuellement calcul�es pr�c�demment
MP_Ranat_Ranc = mergestruct(MP_Ranat_Ranc,MP_Ranat_Ranc_new);
Panc_Rvic = mergestruct(Panc_Rvic,Panc_Rvic_new);



%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               Calcul de la position de r�f�rence
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% chargement �ventuel des points anatomiques dans Rvicon
if exist( fullfile(rep_result_stat, 'P_ANAT_VIC.mat'), 'file' )
    load( fullfile(rep_result_stat, 'P_ANAT_VIC.mat') ) ;
    Panat_Rvic = P_ANAT_VIC ;
else
    Panat_Rvic = struct;
end
% P_ANAT_VIC = Panat_Rvic 		% Points anatomiques dans le rep�re vicon
% calcul de la position de reference 
%   - effort au sol F_ref
%   - Reperes anatomiques
%   - Points anatomiques
%   - Points ancillaires
%   - Points3D (index�s avec les noms des points)
[F_ref, MP_Rvic_Ranat, Panat_Rvic, Panc_Rvic, MP_Ranat_Ranc, points3D] = ...
    reference(static_data, MP_Ranat_Ranc, protocole, Panc_Rvic, Panat_Ranc, Panat_Rvic);


%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           enregistrement des donnees
%         
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
M_ANAT_ANC = MP_Ranat_Ranc ;
P_ANAT_ANC = Panat_Ranc ;
ANC_GEO = Panc_Rvic ;
P_ANAT_VIC = Panat_Rvic ;
R_ANAT_VIC = MP_Rvic_Ranat ;
P_ANC_VIC = Panc_Rvic ;

% Pour r�trocompatibilit� avec anciennes versions mouvement
save( fullfile(rep_result_stat, 'M_ANAT_ANC.mat'), 'M_ANAT_ANC' );
save( fullfile(rep_result_stat, 'P_ANAT_ANC.mat'), 'P_ANAT_ANC' );
save( fullfile(rep_result_stat, 'ANC_GEO.mat'), 'ANC_GEO' );
save( fullfile(rep_result_stat, 'P_ANAT_VIC.mat'), 'P_ANAT_VIC' );
save( fullfile(rep_result_stat, 'F_ref'), 'F_ref' );

save( fullfile(rep_result_stat, 'R_ANAT_VIC'), 'R_ANAT_VIC' ) ;
save( fullfile(rep_result_stat, 'P_ANC_VIC'), 'P_ANC_VIC' ) ;

save( fullfile(rep_result_stat, 'points3D'), 'points3D' );

log_var{ilog} = ['... reference statique calcul�e'] ; ilog = ilog+1;

