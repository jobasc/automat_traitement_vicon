%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%              D�tection des variables n�cessaires au programme
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
log_var{ilog} = ['Programme lanc� : sub_prepa_variables.m'] ; ilog = ilog+1;
log_var{ilog} = ['Patient : ', nom_patient] ; ilog = ilog+1;
log_var{ilog} = ['rep_patient : ', rep_patient] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxx      Fichier protocole
%       Deux cas:
% fichiers C3D d�j� tri�s rep_prot > rep_patient > situations
% fichiers C3D avec classification vicon: rep_prot>rep_classif>rep_patient

% par d�faut, la structure vicon est recherch�e: avec r�pertoire classif
[rep_classif, patient_nom] = fileparts(rep_patient) ;  % rappel: protocole dans r�pertoire contenant les diff�rents patients
[rep_prot, classif_nom] = fileparts(rep_classif) ;

tmp_lst_protfile = dir( fullfile(rep_prot, '*.prot') ) ; % recherche fichier.prot dans structure de type vicon

% si prot non trouv�, recherche dans une structure sans r�pertoire classif
if isempty( tmp_lst_protfile)
    disp( ['fichier protocole absent de : ', rep_prot, ' - recherche dans ', rep_classif] ) ;
    log_var{ilog} = ['fichier protocole absent de : ', rep_prot, ' - recherche dans ', rep_classif] ; ilog = ilog+1;
    classif_nom = '' ;
    tmp_lst_protfile = dir( fullfile(rep_classif, '*.prot') ) ;
    rep_prot = rep_classif ;
    tmp_lst_protfile = dir( fullfile(rep_prot, '*.prot') ) ; % recherche fichier.prot dans structure de type vicon
end

if isempty( tmp_lst_protfile)
    msgbox( ['fichier protocole absent de : ', rep_prot] ) ;
    log_var{ilog} = ['fichier protocole absent de : ', rep_prot] ; ilog = ilog+1;
    return; % arr�t du programme si pas de fichier protocole au bon endroit
else
    prot_file = fullfile( rep_prot, tmp_lst_protfile(1).name) ;  % choix du premier fichier .prot trouv�
    [~, prot_nom, ~] = fileparts(prot_file) ;
    log_var{ilog} = ['protocole: ', prot_file] ; ilog = ilog+1;
end

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%              Lecture protocole
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
log_var{ilog} = ['xxxxxxxxx  lecture fichier protocole'] ; ilog = ilog+1;
protocole = lire_fichier_prot( rep_prot, [prot_nom '.prot'] );
choix_utilisateur.repprotocole = rep_prot ;


log_var{ilog} = ['... fin de lecture fichier protocole'] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxx      Fichier mod�le patient
%       dans la base de donn�es, dans r�pertoire du fichier protocole
tmp_lst_modelfile = dir( fullfile(rep_prot, '*.mod') ) ;
if isempty( tmp_lst_modelfile)
    msgbox( ['fichier modele patient absent de : ', rep_prot] ) ;
    log_var{ilog} = ['fichier modele patient absent de : ', rep_prot] ; ilog = ilog+1;
    %     return; % arr�t du programme si pas de fichier modele patient au bon endroit
else
    model_file = fullfile( rep_prot, tmp_lst_modelfile(1).name) ;  % choix du premier fichier .prot trouv�
    [~, model_nom, ~] = fileparts(model_file) ;
    log_var{ilog} = ['protocole: ', model_file] ; ilog = ilog+1;
end

% xxxxxxxxxxxxxxxxxxxx      R�pertoire des fichiers .plat et .label
rep_platfiles = rep_prot ;   % pour l'instant: dans le m�me r�pertoire que le protocole
log_var{ilog} = ['rep_platfiles: ', rep_platfiles] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxx       Fichier statique
%   si sp�cifi� dans FICHIER_STATIQUE,'repertoire', dans le r�pertoire   sous le r�pertoire patient 
%   si sp�cifi� "_nochange": dans le m�me r�pertoire que session
%   si non sp�cifi� : dans le r�pertoire "fichier_reference" 
% Rappel: un seul fichier statique servant de positionnement des ancillaires
%       et de r�f�rence
if isfield(protocole.FICHIER_STATIQUE,'repertoire')
    switch protocole.FICHIER_STATIQUE.repertoire{1}
        case '_nochange'
            nom_stat_rep = nom_session_seluser;
        otherwise
            nom_stat_rep = protocole.FICHIER_STATIQUE.repertoire{1} ;
    end
else
    nom_stat_rep = 'fichier_reference' ;
end
rep_stat = fullfile(rep_patient, nom_stat_rep) ;

if isfield(protocole.FICHIER_STATIQUE,'reference')
    nom_statref = protocole.FICHIER_STATIQUE.reference{1} ;
    if exist( fullfile( rep_stat, [nom_statref '.c3d']), 'file')
        stat_file{1} = fullfile( rep_stat, [nom_statref '.c3d']) ;
        log_var{ilog} = ['fichier statique: ', stat_file{1}] ; ilog = ilog+1;
    else
        tmp_msg = ['fichier statique sp�cifi� dans le protocole absent  : ', fullfile( rep_stat, [nom_statref '.c3d'])] ;
        msgbox( tmp_msg ) ;
        log_var{ilog} = tmp_msg ; ilog = ilog+1;
        return;
    end
else
    nom_statref = 'fichier_reference' ;
    rep_stat = fullfile(rep_patient, nom_statref) ;  % r�pertoire contenant le fichier statique
    tmp_dir_stat = dir( fullfile(rep_stat, '*.c3d') ) ;     % liste si plusieurs fichiers statiques
    if isempty( tmp_dir_stat)
        msgbox( ['fichier statique absent de : ', rep_stat] ) ;
        log_var{ilog} = ['fichier statique absent de : ', rep_stat] ; ilog = ilog+1;
        return;
    else
        [tmp_lst_stat, tmp_ind] = sort( cell2mat({tmp_dir_stat(:).datenum}) ) ; % tri des fichiers par date et index du tri
        stat_file{1} = fullfile( rep_stat, tmp_dir_stat(tmp_ind(end)).name ) ;     % s�lection du plus r�cent fichier c3d
        log_var{ilog} = ['fichier statique: ', stat_file{1}] ; ilog = ilog+1;
    end
end
% xxxxxxxxxxxxxxxxxxxx       Liste des Situations
%   A partir du protocole
if isfield(protocole,'SITUATIONS')
    lst_situations = fieldnames(protocole.SITUATIONS);
else
    % si pas de situation d�clar�e, prise en compte de tous les r�pertoires
    % sauf r�pertoire statique
    tmp_dir_sit = dir(rep_patient) ;
    lst_situations = setdiff( { tmp_dir_sit([tmp_dir_sit(:).isdir]).name }, {'.', '..', 'Statique'} ) ;
    %     lst_situations = 'none';
end

if size(lst_situations,1) > size(lst_situations,2)
    lst_situations = lst_situations' ;
end

% tmp_lst = dir(rep_patient) ;
% tmp_rep =  {tmp_lst([tmp_lst(:).isdir]).name};
% tmp_reserved = {'.', '..', 'Statique'} ;
% lst_situations = setdiff(tmp_rep, tmp_reserved) ;
log_var{ilog} = ['Situations: ', strjoin(lst_situations,'-')] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxx          Association des changements de plateformes
% .plat avec chaque situation
% chargement des fichiers .plat CERAH et IRR correspondants ds structure associ�e � la situation
% chargement des fichiers variables de chq situation

lst_situations_sel_tmp = {}; % s�lection de situations
for i_situation = 1:size(lst_situations,2)
    mysituation = lst_situations{i_situation} ;
    
    if exist( fullfile( rep_platfiles, [mysituation base_suffix '.plat']), 'file'  )
        sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,[mysituation base_suffix '.plat']);
    else
        sel_platfile.(mysituation) = {} ;
    end
    
    % enl�ve une situation de la liste � traiter si son r�pertoire n'est pas pr�sent
    if exist(fullfile(rep_patient, mysituation), 'dir')
        lst_situations_sel_tmp{end+1} =  mysituation ;
    end
end

lst_situations = lst_situations_sel_tmp ;



% Dans le cas d'une session s�lectionn�e par l'utilisateur (ou d'un
% fichier, les situations se limitent � cette liste
switch traitement_seluser
    case 'Session'
        lst_situations = intersect( lst_situations, nom_session_seluser ) ;
    case 'Essai'
        lst_situations = intersect( lst_situations, nom_session_seluser ) ;
end

clear lst_situations_sel_tmp ;