%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%      Retouche les labels des marqueurs dans tous les fichiers .C3D
%               de toutes les situations
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%  xxxxxxxxxx   FONCTION A DEVELOPPER actuellement: pas de relabellisation
%  avec liste de noms � relabelliser
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% Objectif:
%   - dans les C3D du r�pertoire cible (repertoire d'analyse), modifier les
%   labels des marqueurs
%   - supprime le nom du mod�le dans le libell� si existe
%   - supprime les marqueurs non labellis�s
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire contenant les fichiers .plat des situations � traiter.
%       . Contiennent notamment la position des plateformes de force
%       . FORMAT: devers_CERAH.plat ou pente5_CERAH.plat ou pente5_IRR.plat
%   - r�pertoire de destination
%   - choix des patients � traiter
%   - choix des situations � traiter
%
% En sortie:
%   - r�pertoire "scanbase" � c�t� du r�pertoire initial,
%   	dont le nom est le nom du repertoire initial, avec "scanbase" en
%   	suffixe
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - lst_situations  : toutes situations (plat, d�vers, pente, etc) seront trait�es

log_var{ilog} = ['xxxxxxxxx  pr�traitement fichiers c3d'] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%           Chargement �ventuel du fichier de nouvelle labellisation
% utilisation du premier fichier .relabel trouv�
tmp_lst = dir(fullfile(rep_platfiles,'*.relabel')) ;
if ~isempty(tmp_lst)
    newlabels = lire_fichier_prot(rep_platfiles,tmp_lst(1).name);
    log_var{ilog} = ['... chargement du fichier de relabellisation'] ; ilog = ilog+1;
else
    newlabels = {};
end
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%         It�ration sur les situations (plat, Statique, d�vers, etc.)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

for i_situation = 1:size(lst_situations,2)
    mysituation = lst_situations{i_situation} ;
    mysit_rep = fullfile( rep_patient, mysituation) ;
    log_var{ilog} = ['xxxxxxxxxxx    relabel C3D     Situation ' mysituation] ; ilog = ilog+1;
    
    if strcmp(mysituation, 'Statique')
        mysit_rep_cible = fullfile( patient_courant, 'fichiers_reference') ;
    else
        mysit_rep_cible = fullfile( patient_courant, mysituation, 'fichiers_cinemat') ;
    end
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %           it�ration sur les fichiers .C3D
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % xxxxxxxx          relabel des fichiers si variable newlabels non vide
    
    tmp_lst = dir( fullfile( mysit_rep_cible, '*.c3d' )) ;
    lst_files_to_read = {tmp_lst(:).name};
    
    log_var{ilog} = ['... Relabellisation des fichiers de ' mysit_rep_cible] ; ilog = ilog+1;
    
    for i_file=1:length(tmp_lst)
        file_to_read =  lst_files_to_read{i_file} ;
        
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %
        %           lecture fichier/remplissage liste mkrs
        %
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        [data_acquis, byteOrder, storageFormat] = btkReadAcquisition(file_to_read);
        tmp_mkrs  = btkGetMarkers(data_acquis) ;    % extraction de la structure liste des mkrs de l'acquisition
        lst_mkrs_acquis = fieldnames(tmp_mkrs) ;  % extraction de la liste des marqueurs de l'acquisition
        

        
        
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %
        %           Nettoyage du nom sujet dans le nom marqueur
        %
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
        % xxxxxxxxxxx       A d�velopper        xxxxxxxxxxxxxxxxxxxxxxxxxxxx
        % si la liste des mkrs cible est identifi�e
        %          [cleaned_lst_mkrs] = sub_clean_mkrs_names(lst_mkrs_to_rename, lst_mkrs_cible, nom_patient)
        % xxxxxxxxxxx       A d�velopper        xxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
        [cleaned_lst_mkrs] = sub_clean_mkrs_names( sel_mkrs ) ;
        
        
        
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %
        %           relabel �ventuel avec liste de relabellisation
        %
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %             relabel_c3d_irr_vers_lbm(file_to_read, mysit_rep_cible, newlabels) ;
        
        
        % % %         if ~isempty( newlabels)
        % % %             liste_labels_a_renommer = fieldnames(LABELS.RENAME); % liste de marqueurs � relabelliser
        % % %             errorlabels = relabel_c3d(fullfile( mysit_rep_cible, file_to_read) , newlabels) ;
        % % %             log_var{ilog} = ['... Erreurs de label: ' errorlabels] ; ilog = ilog+1;
        % % %         end    % if ~isempty( newlabels)
        
          % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %
        %           r��criture du fichier c3d avec nvx mkrs
        %
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
    end
    
end

log_var{ilog} = ['xxxxxxxxx  fin relabel fichiers c3d'] ; ilog = ilog+1;

