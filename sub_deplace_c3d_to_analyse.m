%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           pr� traitement fichiers c3D (ptf, emplacements
%                   (�quivalent de scan_base mouvement)
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - d�placement des fichiers c3d du r�pertoire vicon vers le r�pertoire
%   analyse, dont la structure peut �tre lue avec mouvement
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire de destination
%   - choix des patients � traiter
%   - choix des situations � traiter
%
% En sortie:
%   - r�pertoire analyse rempli avec les fichiers
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables issues du programme appelant: "Main_automat_VICON.m"
%
%   - lst_situations  : toutes situations (plat, d�vers, pente, etc) seront trait�es

log_var{ilog} = ['xxxxxxxxx  remplissage analyse avec c3d'] ; ilog = ilog+1;

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                  �criture dans les r�pertoires d'analyse _ fichiers statiques
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% d�placement des fichiers statiques
mysit_rep = fullfile( rep_patient_base_scanned, nom_stat_rep) ;
mysit_rep_cible = fullfile( patient_courant, 'fichiers_reference') ;

% xxxxxxxxxxxx      Transfert de fichiers vers r�pertoire cible
if ~isempty( dir( fullfile( mysit_rep, '*.c3d') ) )
    if ~exist( mysit_rep_cible, 'dir')
        mkdir( mysit_rep_cible );
        log_var{ilog} = ['... Cr�ation de ' mysit_rep_cible] ; ilog = ilog+1;
    end
    copyfile( fullfile( mysit_rep, '*.c3d'), mysit_rep_cible) ;
    log_var{ilog} = ['... Copie de fichiers vers ' mysit_rep_cible] ; ilog = ilog+1;
else
    disp( ['... pas de fichier c3d dans ' mysit_rep_cible] ) ;
    log_var{ilog} = ['... pas de fichier c3d dans ' mysit_rep_cible] ; ilog = ilog+1;
end

% xxxxxxxxxxx   It�ration sur les situations (plat, d�vers, etc.)
for i_situation = 1:size(lst_situations,2)
    mysituation = lst_situations{i_situation} ;
    mysit_rep = fullfile( rep_patient_base_scanned, mysituation) ;
    log_var{ilog} = ['xxxxxxxxxxx    Modif C3D     Situation ' mysituation] ; ilog = ilog+1;
    
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %                  �criture dans les r�pertoires d'analyse
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % xxxxxxxxxxxx     D�finition du r�pertoire cible
    
    mysit_rep_cible = fullfile( patient_courant, mysituation, 'fichiers_cinemat') ;
    
    % xxxxxxxxxxxx      Transfert de fichiers vers r�pertoire cible
    if ~isempty( dir( fullfile( mysit_rep, '*.c3d') ) )
        if ~exist( mysit_rep_cible, 'dir')
            mkdir( mysit_rep_cible );
            log_var{ilog} = ['... Cr�ation de ' mysit_rep_cible] ; ilog = ilog+1;
        end
        copyfile( fullfile( mysit_rep, '*.c3d'), mysit_rep_cible) ;
        log_var{ilog} = ['... Copie de fichiers vers ' mysit_rep_cible] ; ilog = ilog+1;
    else
        disp( ['... pas de fichier c3d dans ' mysit_rep] ) ;
        log_var{ilog} = ['... pas de fichier c3d dans ' mysit_rep] ; ilog = ilog+1;
    end
end

log_var{ilog} = ['xxxxxxxxx  fin remplissage analyse avec c3d'] ; ilog = ilog+1;

