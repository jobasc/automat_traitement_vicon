function Deplace_ptf_ds_c3d( file_to_read, desc_PFV, file_to_create, seuil_nan, seuil_parasites)
%
% Objectif
%   - D�placement des plateformes d'un fichier c3d
%   - ex d'application : escalier, d�vers, pente, etc.
%
% En entr�e:
%   - file: chemin et nom de fichier du fichier .c3d � traiter
%   - desc_PFV: structure contenant
%       .PFVirtuelle : nom des plateformes virtuelles, coins et centres
%       associ�s, au format VICON
%       .corrPF : correspondance entre plateformes r�elles et virtuelles
%   - file_to_create:   chemin et nom complet du fichier � cr�er,
%                       si non saisi, �crasement du fichier source
%   -seuil_nan: seuil d'effort normal en dessous duquel le signal est
%   consid�r� comme nul
%   - seuil_parasites: seuil d'effort normal en dessous duquel le signal
%   est consid�r� comme parasite et annul�
%
% xxxxxxxxxxxxxxxxxx
%           ex de desc_PFV: pente12_APSIC_CERAH.plat
%
% #PFVirtuelle
% PFV1=607, 0, 435, 607, 464, 435, 1115, 464, 494, 1115, 0, 494
% PFV2=508, 464, 423, 508, 0, 423, 0, 0, 364, 0, 464, 364
% PFV3=560, -6, 429, 560, -406, 429, -40, -406, 355, -40, -6, 355
% PFV4=565, -406, 431, 565, -6, 431, 1165, -6, 502, 1165, -406, 502
%
% #corrPF
% PFR1=1
% PFR2=2
% PFR3=3
% PFR4=4
% xxxxxxxxxxxxxxxxxx
%
% En sortie:
%   - fichier c3d modifi�
%       . sp�cifi� dans file_to_create
%       . ou par d�faut: remplace le fichier initial

if nargin < 3
    file_to_create = file_to_read ;
end

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
disp( '   programme Deplace_ptf_ds_c3d ' ) ;
disp( file_to_read );
%       lecture des c3d et extraction des actions mecaniques
fichier = lire_donnees_c3d( file_to_read );
actmec_reelle = fichier.actmec;

%       donnees des plateformes (coins)
h=btkReadAcquisition(file_to_read);
FORCEPLATES = btkGetForcePlatforms(h);

% Calcul des actions virtuelles des plateformes virtuelles et des coins
% des plateformes virtuelles
if ~exist('seuil_nan', 'var')
    seuil_nan = 20 ;
end
if ~exist('seuil_parasites', 'var')
    seuil_parasites = 400 ;
end
[actmec_virtuelle, corners] = plateforme_virtuelle( actmec_reelle, FORCEPLATES, desc_PFV, seuil_nan, seuil_parasites);


% Reecriture du fichier avec les nouvelles plateformes virtuelles
h=btkReadAcquisition(file_to_read);
btkClearAnalogs(h);
btkRemoveMetaData(h, 'FORCE_PLATFORM');
btkAppendMetaData(h, 'ANALOG', 'FORMAT', btkMetaDataInfo('Char', {'SIGNED'}));

nbre_PFvirt=length(fieldnames(desc_PFV.PFVirtuelle)); % pour savoir le nombre de PFV
for ii=1:nbre_PFvirt
    btkAppendForcePlatformType2_Rlocal(h, actmec_virtuelle(:,1+6*(ii-1):3+6*(ii-1)), actmec_virtuelle(:,4+6*(ii-1):6+6*(ii-1)), reshape(corners(ii,:),3,4)')
end

%       �criture du fichier
btkWriteAcquisition(h,file_to_create);

disp( '   ... fin programme Deplace_ptf_ds_c3d ') ;
