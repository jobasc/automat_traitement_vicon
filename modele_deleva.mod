#piedD
methode=deleva
extremite_proximale=MLD,MMD
extremite_distale=MT1D,MT5D
origine=distale


#piedG
methode=deleva
extremite_proximale=MLG,MMG
extremite_distale=MT1G,MT5G
origine=distale

#tibiaD
methode=deleva
extremite_proximale=CLD,CMD
extremite_distale=MLD,MMD
origine=distale

#tibiaG
methode=deleva
extremite_proximale=CLG,CMG
extremite_distale=MLG,MMG
origine=distale

#femurD
methode=deleva
extremite_proximale=TfemD
extremite_distale=CLD,CMD
origine=distale

#femurG
methode=deleva
extremite_proximale=TfemG
extremite_distale=CLG,CMG
origine=distale

#bassin
methode=deleva
extremite_proximale=EPSD,EPSG
extremite_distale=TfemG,TfemD
origine=proximale

#tronc
methode=deleva
extremite_proximale=AG,AD
extremite_distale=EPSD,EPSG
origine=proximale

#brasD
methode=deleva
extremite_proximale=AD
extremite_distale=COUDD
origine=proximale

#brasG
methode=deleva
extremite_proximale=AG
extremite_distale=COUDG
origine=proximale

#avtbrasD
methode=deleva
extremite_proximale=COUDD
extremite_distale=MC3D
origine=proximale


#avtbrasG
methode=deleva
extremite_proximale=COUDG
extremite_distale=MC3G
origine=proximale

#tete
methode=deleva
extremite_proximale=C7
extremite_distale=HV
origine=distale