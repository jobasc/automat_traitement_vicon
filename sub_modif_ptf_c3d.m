%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%                           pr� traitement fichiers c3D
%                               Modif Infos PTF
%
%                               SCRIPT
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% Objectif:
%   - Modif emplacement ptf dans c3d (notamment cr�ation de plateformes
%   virtuelles dans pentes, escaliers, d�vers)
%
% En entr�e:
%   - r�pertoire contenant les donn�es vicon
%   - r�pertoire contenant les fichiers .plat des situations � traiter.
%       . Contiennent notamment la position des plateformes de force
%       . FORMAT: devers_CERAH.plat ou pente5_CERAH.plat ou pente5_IRR.plat
%   - r�pertoire de destination
%   - choix des patients � traiter
%   - choix des situations � traiter
%
% En sortie:
%   - fichier c3d initial sauvegard� dans r�pertoire "sav_c3d_initiaux"
%   - remplacement fichier c3d initial par c3d dont ptf sont modifi�es
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% variables isssues du programme appelant: "Main_automat_VICON.m"
%
%   - lst_situations  : toutes situations (plat, d�vers, pente, etc) seront trait�es

log_var{ilog} = ['xxxxxxxxx  pr�traitement fichiers c3d'] ; ilog = ilog+1;


% xxxxxxxxxxx   It�ration sur les situations (plat, Statique, d�vers, etc.)
for i_situation = 1:size(lst_situations,2)
    mysituation = lst_situations{i_situation} ;
    mysit_rep = fullfile( rep_patient_base_scanned, mysituation) ;
    log_var{ilog} = ['xxxxxxxxxxx    Modif C3D     Situation ' mysituation] ; ilog = ilog+1;
    
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %                  changement de rep�re des plateformes 
    %                           dans fichiers .c3d SOURCES !
    %                   (si pr�sence de fichiers .plat)
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% % % %     ok_sit = 0 ;        % v�rification si on a trouv� le .plat correspondant � la situation
% % % %     
% % % %     if exist( fullfile(rep_platfiles, [mysituation base_suffix '.plat']), 'file'  )
% % % %     
% % % %     if ~isempty( strfind( mysituation, 'pente5') )
% % % %         sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,['pente5_' prot_nom '.plat']);
% % % %         ok_sit = 1 ;
% % % %     end
% % % % %     if ~isempty( strfind( mysituation, 'plat') )
% % % % %         sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,['plat_' prot_nom '.plat']);
% % % % %         ok_sit = 1 ;
% % % % %     end
% % % %     if ~isempty( strfind( mysituation, 'devers') )
% % % %         sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,['devers_' prot_nom '.plat']);
% % % %         ok_sit = 1 ;
% % % %     end
% % % %     if ~isempty( strfind( mysituation, 'pente12') )
% % % %         sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,['pente12_' prot_nom '.plat']);
% % % %         ok_sit = 1 ;
% % % %     end
% % % %     if ~isempty( strfind( mysituation, 'escalier') )
% % % %         sel_platfile.(mysituation) = lire_fichier_prot(rep_platfiles,['escalier_' prot_nom '.plat']);
% % % %         ok_sit = 1 ;
% % % %     end
% % % %     
% % % %     % xxxxxxxx          si la situation n'est pas trouv�e, stockage du message dans le  protocole
% % % %     if ok_sit == 0;
% % % %         sel_platfile.(mysituation) = {} ;
% % % %     end
    
    % xxxxxxxx          Modification des informations plateformes de force
    %                           dans le fichier source!
    if ~isempty( sel_platfile.(mysituation) )
        % liste des fichiers dont les infos ptf sont � transformer
        tmp_lst = dir( fullfile( mysit_rep, '*.c3d' )) ;
        lst_files_to_read = {tmp_lst(:).name};
        
        % partie annul�e: on modifie dans base_scanned
% % %         % xxxxxxx      Cr�ation du r�pertoire de fichiers sources, non retouch�s
% % %         savec3d_rep = fullfile( mysit_rep,'sav_c3d_initiaux');
% % %         if ~exist(savec3d_rep, 'dir') ; mkdir( savec3d_rep ) ; end % cr�ation du r�pertoire sav si n�cessaire
% % %         if ~isempty( dir(fullfile( mysit_rep, '*.c3d')) )
% % %             copyfile(fullfile( mysit_rep, '*.c3d'), savec3d_rep ) ; % sauvegarde des fichiers sources c3d
% % %         end
        
        % xxxxxxx       Modification des donn�es ptf des fichiers c3d du r�pertoire
        for ifile = 1:length(lst_files_to_read)
            disp( ['modif c3d de:' lst_files_to_read{ifile} ]);
            file_to_read = fullfile( mysit_rep, lst_files_to_read{ifile} );
            Deplace_ptf_ds_c3d( file_to_read, sel_platfile.(mysituation) ) ;
        end
    end
    
end

log_var{ilog} = ['xxxxxxxxx  fin pr�traitement fichiers c3d'] ; ilog = ilog+1;

