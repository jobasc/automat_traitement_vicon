% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           Programme automatique de traitement de donn�es VICON
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
% Note: � utiliser pour traiter automatiquement les donn�es d'un r�pertoire
% d'une session vicon VICON
%
% Demand� � l'utilisateur:
%   - R�pertoire du Patient � traiter
%
% Contenu d'acquisition attendu
%   - R�pertoire patient > session statique: nomm�e "Statique"
%       Choix: UN SEUL fichier statique servant de positionnement des ancillaires et de r�f�rence
%   - R�pertoire patient > situations (plat, pente, escalier, devers, etc.)
%
% Autres contenus attendus:
%   - fichier protocole : s�lectionn� selon la session de mesure (plat, pente, escalier, d�vers, etc.),
%                   -> dans le r�pertoire parent du patient
%   - fichiers .plat, pour le changement de rep�res plateformes(pente5_nomprotocole.plat, pente12_nomprotocole.plat, etc.)
%                   -> dans le r�pertoire parent du patient
%                   -> attention : le nom du protocole doit suivre la situation d�crite: pente5_nomprotocole.plat
%   - fichier de LABELS : s'il faut renommer les marqueurs
%                   -> dans le r�pertoire parent du patient
%
% Structure:
% BASE DE DONNEES (contient .enf et .eni)
%  Classif 1
%   Patient1
%       Statique
%           Statique.c3d
%       plat
%           marcheA1.c3d
%           marcheA2.c3d
%           marcheA3.c3d
%       devers
%           marcheB1.c3d
%           marcheB2.c3d
%       pente
%           ...
% xxxxxxxxxxxxxxxxxxxxxxxxxx
%	Patient2
%       Statique
%           Statique.c3d
%       plat
%           marcheA1.c3d
%           marcheA2.c3d
%           marcheA3.c3d
%       devers
%           marcheB1.c3d
%           marcheB2.c3d
%       pente
%           ...
% xxxxxxxxxxxxxxxxxxxxxxxxxx
%   ANALYSE
%       patient1
%           plat
%               traitement
%                     fichiers matlab r�sultat
%               resultats
%                       excel r�capitulatif r�sultats
%           devers
%               traitement
%                     fichiers matlab r�sultat
%               resultats
%                       excel r�capitulatif r�sultats
%           pente
%               ...
%       patient2
%           plat
%               traitement
%                     fichiers matlab r�sultat
%               resultats
%                       excel r�capitulatif r�sultats
%           devers
%               ...
% **********
%   En Sortie
%   - R�pertoire "Analyse", cr�� � la racine de la base de donn�es, 1 rep
%   par patient et situation
%   - Restitutions excel, une par session de mesure du patient
%   - Rapport �ventuel si sp�cifi� dans protocole
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%       Modifications
%
% bascou 18/08/2015: cr�ation initiale

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%              Donn�es � remplir par l'utilisateur
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
% % d�sactive l'interpr�teur de texte pour tout le monde
%  set(0, 'DefaulttextInterpreter', 'none');
 
% r�pertoire patient - s�lection utilisateur
traitement_seluser = questdlg('Traiter 1) toutes les situations pour un patient, 2) une session particuli�re (plat, pente, etc.) ou 3) un essai particulier (marche01)?', 'choix traitement', 'Patient', 'Session', 'Essai', 'Patient') ;

switch traitement_seluser
    case 'Patient'
        rep_patient = uigetdir('','S�lectionner le r�pertoire contenant les sessions de passage du patient');
        [tmp, nom_patient] = fileparts( rep_patient) ;
    case 'Session'
        rep_session_seluser = uigetdir('','S�lectionner le r�pertoire de la session (plat, pente, etc) du patient � traiter');
        [rep_patient, nom_session_seluser] = fileparts( rep_session_seluser) ;
        [tmp, nom_patient] = fileparts( rep_patient) ;
    case 'Essai'
        [file_essai_seluser, rep_essai_seluser] = uigetfile('','S�lectionner le fichier c3d de l essai � traiter','*.c3d');
        [rep_session, nom_essai_seluser, suffix_essai_seluser] = fileparts( fullfile(rep_essai_seluser, file_essai_seluser) ) ;
        [rep_patient, nom_session_seluser] = fileparts( rep_session) ;
        [tmp, nom_patient] = fileparts( rep_patient) ;
    otherwise
        break
        disp(' pas de choix effectu�, programme arr�t�') ;
end

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%       MAJ barre avancement
waitb_h = waitbar(0,'Initialisation', 'OuterPosition', [200,400,300,100], 'Color', [0.8 0.8 1]) ;
% change the interpreter of the String
h_tmp=findobj(waitb_h,'Type','figure');
ht_tmp = get(get(h_tmp,'currentaxes'),'title');
set(ht_tmp,'Interpreter','none');

% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

% testing: H:\01-projets\FRM_propulsion\2015_siegel\4_mesures\FRM_ARS\0_Bdd_Vicon\FRM_ARS\FRM_ARS_PhaseA

% genre du patient (pour application De Leva) - annul� pour l'instant
patient_data.Questionnaire.Sexe = questdlg('Patient Masculin (M) ou F�minin (F) ?', 'Questionnaire patient', 'M', 'F', 'M') ;

% Type de base de c3d : APSIC ou IRR
base_suffix = '_APSIC_CERAH' ;

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%               log files
%
% fichier et variable log
log_var = cell(1000,1);
ilog = 1;

%% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%              D�tection des variables n�cessaires au programme
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
run('sub_prepa_variables.m') ;




% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
%
%           Cr�ation des r�pertoires de travail (analyse, resultat, etc.)
%
% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

try
    % % % Excel r�sultat   % d�clar� apr�s
    % % xls_result_file = fullfile( rep_analyse, 'results_analyse.xls') ;
    % % log_var{ilog} = ['xls_result_file: ', xls_result_file] ; ilog = ilog+1;
    
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %              Modification des c3d et organisation des donn�es
    %               (�quivalent de scan_base mouvement)
    %
    %   modification : relabel (enlever pr�fixe nom mod�le+ relabel �ventuel)
    %   d�placement: dans analyse/traitement/patient
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % ex�cution du script
    run('sub_copie_dans_base_scanned');
    run('sub_crea_rep_init.m') ;
    run('sub_modif_ptf_c3d') ;
    run('sub_deplace_c3d_to_analyse') ;
    run('sub_deplace_protvar_to_analyse') ;
    
    % xxxxxxxxxxx     a developper si besoin   xxxxxxxxxxxxxxxxxxxx
%     run('sub_retouche_labels_c3d'); 
%     run('sub_rename_mkrs_c3d') ;
    
    
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %              chargement des fichiers variables de chq situation
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    log_var{ilog} = ['xxxxxxxxx  lecture fichiers variables'] ; ilog = ilog+1;
    
    for i_situation = 1:size(lst_situations,2)
        mysituation = lst_situations{i_situation} ;
             
        % lecture du fichier variable
        if isfield( protocole.SITUATIONS, mysituation ) && exist( fullfile( rep_prot, [ protocole.SITUATIONS.(mysituation){1} '.var'] ), 'file' )
            sel_varfile.(mysituation) = lire_fichier_variable( rep_prot, fullfile( rep_prot, [ protocole.SITUATIONS.(mysituation){1} '.var'] ) ) ;
        else
            sel_varfile.(mysituation) = {} ;
        end
    end
    
    log_var{ilog} = ['... fin de lecture fichiers variables'] ; ilog = ilog+1;
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %              Lecture et traitement fichier statique
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    run('sub_lecture_fichier_statique') ;
    
    run('sub_calc_anc_et_statref') ;
    
    % xxxxxxxxxxxxxxxxxxxx          liste des segments
    list_segments = fieldnames(protocole.SEGMENTS);
    
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %
    %              Modele
    %
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %       Lecture du fichier modele
    
    % choix: pas d'inertie utilis�e, pas besoin de calculer le mod�le
    inert_choice = 'sans_inerties' ;
    avecinertie = 0;
    
    % pas de mod�le cr�� si inertie non utilis�e
    if avecinertie
        run('sub_crea_modele') ;
    end
    
    
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %              ITERATION     -   SITUATIONS
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    for i_situation = 1:size(lst_situations, 2)   %  it�ration conditions (plat, d�vers, pente etc.)
        
        try
        mysituation = lst_situations{i_situation} ;
        mysit_dir = fullfile( patient_courant, mysituation, 'fichiers_cinemat') ; % chemin de la situation
        
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        %       MAJ barre avancement
        waitbar((i_situation-1)/ size(lst_situations,2), waitb_h, ['situation : ' mysituation ' - ' num2str(i_situation) ' / ' num2str(size(lst_situations,2)) ] ) ;
        % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        
        disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') ;
        disp( [ '       traitement situation :   ' mysituation ] ) ;
        disp('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') ;
        
        tmp_lst = dir( fullfile( mysit_dir, '*.c3d' ) );    % lst des fichiers c3d du chemin
        
        
        if ~isempty(tmp_lst) %  pas de traitement si pas de c3d
            lst_sit_c3dfiles = { tmp_lst(:).name} ;
            lst_sit_c3dfilesfull = fullfile( mysit_dir, lst_sit_c3dfiles) ;     % chemin complet des fichiers c3d
            
            
            % Dans le cas d'une fichier sp�cifique s�lectionn� par l'utilisateur
            switch traitement_seluser
                case 'Essai'
                    lst_sit_c3dfiles = intersect( lst_sit_c3dfiles, file_essai_seluser ) ;
                    lst_sit_c3dfilesfull = intersect( lst_sit_c3dfilesfull, fullfile(mysit_dir, file_essai_seluser) ) ;
                    
            end
            %   R�pertoire de travail r�sultats cin�matiquess
            rep_res_cin = fullfile( patient_courant, mysituation, 'resultats_cinemat' ) ;
            
            
            %       chargement de la variable associ�e � la situation
            lst_variables_desc  = sel_varfile.(mysituation) ;
            lst_variables = fieldnames(lst_variables_desc) ;
            
            %        Cr�ation du fichier    kinematic_data
            kinematic_data = lecture_donnees_cinematique( lst_sit_c3dfilesfull );
            
            if ~exist( rep_res_cin ,'dir');
                mkdir( rep_res_cin ) ;
            end
            
            
            save( fullfile( rep_res_cin, 'kinematic_data.mat'), 'kinematic_data' );
            log_var{ilog} = ['xxxxxxxxx fin lecture fichier cinematique de la situation ' mysituation] ; ilog = ilog+1;
            
            %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %              ITERATION     -   TRIALS
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            lst_trials = fieldnames(kinematic_data);
            lst_trials_full = fullfile( rep_res_cin, lst_trials) ;
            
            waitb_file_h = waitbar(0,'Traitement des fichiers', 'OuterPosition', [200,200,250,100], 'Color', [1 0.8 0.8], 'Interpreter', 'none') ;
            % change the interpreter of the String
            h_tmp=findobj(waitb_file_h,'Type','figure');
            ht_tmp = get(get(h_tmp,'currentaxes'),'title');
            set(ht_tmp,'interpreter','none')
            
            
            for itrial = 1:length(lst_trials)   % it�ration fichiers cin�matiques
                
                
                mytrial = lst_trials{itrial} ;
                % xxxxxxxx      affichage barre avancement fichier
                waitbar((itrial-1)/length(lst_trials), waitb_file_h, ['Fichier en cours: ' mytrial ' - ' num2str(itrial) ' / ' num2str(length(lst_trials)) ] ) ;
                
                disp('   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') ;
                disp(['     Essai : ' mytrial]) ;
                disp('   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') ;
                
                mytrialdir = fullfile( rep_res_cin, mytrial) ;
                if ~exist( mytrialdir, 'dir')
                    mkdir( mytrialdir) ;
                end
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %              Traitements cinematique
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                
                run('sub_kinematics') ;
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %       Calcul des r�sultats de la cin�matique
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinematics_calc_results') ;
                
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %            calcul des matrices legnani cinetiques - s'il existe modele
                %            pseudo inertie, CdG, vitesse CdG, qte mvt, qte acc�leration
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinetics_qtes_legnani') ;
                
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %               Calcul des efforts ext�rieurs (sur plateformes
                %       Calcul des efforts externes, cop, etc.
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinetics_calc_efforts_ext') ;
                
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %               D�tection des cycles
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinetics_cycle_detect') ;
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %               Calcul des efforts externes sens salle, cop, etc.
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinetics_calc_other_ptf_values') ;
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %            calcul efforts par dynamique inverse
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                if isfield(protocole,'CHAINES_DYNAMIQUE_INVERSE')
                    run('sub_kinetics_inverse_dynamics') ;
                    
                else
                    disp('pas de calcul de dynamique inverse d�finies dans le .prot');
                end;
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %       Calcul des r�sultats de la dynamique
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                run('sub_kinetics_calc_results') ;
                
                
                
                %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                %
                %           param�tres g�n�riques de la marche
                %
                % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                
                
                run('sub_Mresults_calc_param_marche') ;
                
                
                % nettoyage des donn�es du trial
                clear( 'R_ANC_VIC_t', 'A_ANAT_VIC_t');
                clear( 'H_ANAT_VIC_t', 'MP_Rsalle_Ranat_t', 'MP_Rvic_Ranat_t', 'MP_Rvic_Ranc_t', 'MP_Rvic_Rsalle_t' );
                clear( 'M_ANAT_ANC', 'M_VIC_ANAT_t', 'P_ANAT_VIC_t', 'P_ANC_VIC_t', 'Panat_Rvic_t', 'Panc_Rvic_t') ;
                clear( 'R_ANAT_VIC_t', 'R_SALLE_VIC_t', 'W_ANAT_VIC_t') ;
                clear( 'actions_meca', 'actmec_sel', 'axe_R_VIC', 'cycle', 'effort_ref') ;
                clear( 'newlabels', 'noms_champ_cycle', 'ok_sit', 'point_controle') ;
                clear( 'points3D', 'points3D_t', 'pos_seg_orient_tmp1', 'pos_seg_orient_tmp2');
                clear( 'res_actions_meca', 'res_angles_t', 'res_cop', 'res_effort', 'res_effort_articulaire', 'res_moment', 'res_moment_articulaire') ;
                clear( 'res_points3D', 'res_var_angle_t', 'res_variable')
                
                
            end   % for itrial
            close(waitb_file_h) ;
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %              FIN ITERATION     -   TRIALS
            %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            
            
            %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %
            %               normalisation
            %
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            run('sub_posttreat_normalisation') ;
            run('sub_posttreat_mean_calc') ;
            
            %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            %
            %              ecriture excel
            %
            % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            run('sub_posttreat_ecriture_xls') ;
            
            
            
        else
            disp(' Attention, pas de fichier c3d pour la situation')
        end
        % nettoyage des donn�es de la situation
        clear( 'lst_variables_desc', 'kinematic_data', 'lst_variables', 'rep_res_cin') ;
        
        catch
            disp(['erreur dans situation' mysituation] ) ;
        end
    end   % for isituation
    close( waitb_h ) ;
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %              FIN ITERATION     -   SITUATION
    %% xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           ecriture fichier log final
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % ouverture du fichier cible
    log_file = fullfile( rep_patient_cible, 'log_traitement_patient.txt' ) ;
    fid = fopen(log_file, 'w') ;
    % ecriture des libell�s (boucle n�cessaire car les cellules ne peuvent
    % �tre �crites que ligne par ligne
    for iline = 1:size(log_var,1)
        fprintf(fid, '%s\r\n', log_var{iline,:}) ;
    end
    %       ecriture des matrices
    fprintf(fid, format_result_tmp, mat_result) ;
    %       fermeture du fichier
    fclose(fid) ;
    
catch err
    log_file = fullfile( rep_patient_cible, 'log_traitement_patient.txt' ) ;
    
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!') ;
    disp( ' ') ;
    disp( '         Arret suite � une erreur - voir log file' );
    disp( log_file );
    disp( ' ') ;
    disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!') ;
    rethrow(err);
    
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    %           ecriture fichier log final
    % xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    % ouverture du fichier cible
    fid = fopen(log_file, 'w') ;
    % ecriture des libell�s (boucle n�cessaire car les cellules ne peuvent
    % �tre �crites que ligne par ligne
    for iline = 1:size(log_var,1)
        fprintf(fid, '%s\r\n', log_var{iline,:}) ;
    end
    %       ecriture des matrices
    fprintf(fid, format_result_tmp, mat_result) ;
    %       fermeture du fichier
    fclose(fid) ;
    
    
end

