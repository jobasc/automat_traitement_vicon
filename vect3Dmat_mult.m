function PR1 = vect3Dmat_mult(MP_R1_R2, PR2)
PR1 = zeros( size(PR2) ) ;

for itps = 1:size(MP_R1_R2, 3)
    PR1(:,:,itps) = MP_R1_R2(:,:,itps) * PR2(:,:,itps) ;
end